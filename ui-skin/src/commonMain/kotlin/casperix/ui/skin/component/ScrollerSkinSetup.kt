package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.interpolation.easeInOutCubic
import casperix.math.interpolation.interpolateFunction
import casperix.math.vector.Vector2d
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.scroll.ScrollerAnimationSettings
import casperix.ui.component.scroll.ScrollerSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class ScrollerSkinSetup(supplier: Supplier, storage: HierarchicalStorage, panelSkinSetup: PanelSkinSetup) {
	val scrollBorder = SideIndents(6)
	val scrollerSkin = ScrollerSkin(scrollBorder, Vector2d(16.0),
		panelSkinSetup.panelBack,
		supplier.image9Slice(FileReference.classpath("graphics/scroller/border.png"), 3.0, 0.0),
		supplier.imageStretch(FileReference.classpath("graphics/scroller/shadow.png")),
		ScrollerAnimationSettings(Vector2d.interpolateFunction(easeInOutCubic)) { 0.200 })

	init {
		storage.set(scrollerSkin)
	}
}