package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.color.Color
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.ButtonDrawType
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.button.ButtonSpecularSkin
import casperix.ui.component.button.ButtonStateSkin
import casperix.ui.component.checkbox.CheckBoxSkin
import casperix.ui.component.text.TextShadowSettings
import casperix.ui.component.text.TextSkin
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier
import casperix.ui.type.LayoutSide

class ButtonsSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val buttonGap = SideIndents(6)
	val tabButtonGap = SideIndents(6)

	val labelDisabled = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.BLACK.setAlpha(0.4))
	val labelDefaultShadow = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.WHITE.setAlpha(0.8), shadow = TextShadowSettings(Vector2d(0.0, 2.0), Color.BLACK.setAlpha(1.0)))
	val labelSmallShadow = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.WHITE.setAlpha(0.8), shadow = TextShadowSettings(Vector2d(0.0, 1.0), Color.BLACK.setAlpha(1.0)))


	val buttonSkin = create9SliceButtonSkin(supplier, "graphics/button/", 5.0, 0.0)
	val toggleSkin = ToggleSkin(
		create9SliceButtonSkin(supplier, "graphics/button/toggle/on/", 7.0, -1.0),
		create9SliceButtonSkin(supplier, "graphics/button/toggle/off/", 5.0, 0.0),
	)
	val checkBoxSkin = CheckBoxSkin(
		ToggleSkin(
			create9SliceButtonSkin(supplier, "graphics/button/checkbox/on/"),
			create9SliceButtonSkin(supplier, "graphics/button/checkbox/off/"),
		)
	)


	init {
		storage.set(buttonSkin)
		storage.set(toggleSkin)
		storage.set(checkBoxSkin)
	}

	fun createTabToggle(supplier: Supplier, buttonSide: LayoutSide): ToggleSkin {
		val subName = buttonSide.name.lowercase()
		return ToggleSkin(
			create9SliceButtonSkin(supplier, "graphics/button/tab/$subName/on/", 11.0, -4.0, buttonSide),
			create9SliceButtonSkin(supplier, "graphics/button/tab/$subName/off/", 11.0, -4.0, buttonSide),
		)
	}

	fun create9SliceButtonSkin(
		supplier: Supplier,
		prefix: String,
		indents: Double,
		border: Double = 0.0,
		buttonSide: LayoutSide? = null,
		textOffsetOnPress: Vector2i? = null,
		drawType: ButtonDrawType? = null,
		hitBorder: SideIndents? = null,
		pressedLabelCurrent: TextSkin = labelSmallShadow,
		normalLabelCurrent: TextSkin = labelDefaultShadow,
	): ButtonSkin {
		return create9SliceButtonSkin(supplier, prefix, SideIndents(indents), SideIndents(border), buttonSide, textOffsetOnPress, drawType, hitBorder, pressedLabelCurrent, normalLabelCurrent)
	}

	fun create9SliceButtonSkin(
		supplier: Supplier,
		prefix: String,
		indents: SideIndents = SideIndents.ZERO,
		border: SideIndents = SideIndents.ZERO,
		buttonSide: LayoutSide? = null,
		contentOffsetOnPress: Vector2i? = null,
		drawType: ButtonDrawType? = null,
		hitBorder: SideIndents? = null,
		pressedLabelCurrent: TextSkin = labelSmallShadow,
		normalLabelCurrent: TextSkin = labelDefaultShadow,
	): ButtonSkin {
		val sideTabGap = 0.0
		val buttonGap = when (buttonSide) {
			null -> buttonGap
			LayoutSide.LEFT -> tabButtonGap.copy(right = sideTabGap)
			LayoutSide.RIGHT -> tabButtonGap.copy(left = sideTabGap)
			LayoutSide.BOTTOM -> tabButtonGap.copy(top = sideTabGap)
			LayoutSide.TOP -> tabButtonGap.copy(bottom = sideTabGap)
		}
		val allowClipping = buttonSide == null

		return ButtonSkin(
			allowClipping,
			buttonGap, SideIndents(0),
			ButtonStateSkin(supplier.image9Slice(FileReference.classpath("${prefix}disabled.png"), indents, border), labelDisabled),
			ButtonStateSkin(supplier.image9Slice(FileReference.classpath("${prefix}normal.png"), indents, border), normalLabelCurrent),
			ButtonStateSkin(supplier.image9Slice(FileReference.classpath("${prefix}focused.png"), indents, border), normalLabelCurrent),
			ButtonStateSkin(supplier.image9Slice(FileReference.classpath("${prefix}pressed.png"), indents, border), pressedLabelCurrent),
			ButtonSpecularSkin(
				supplier.imageStretch(FileReference.classpath("graphics/button/specular.png")),
				Vector2d(256.0),
			),
			drawType ?: ButtonDrawType.SMOOTHED_LAYERS,
			hitBorder ?: SideIndents.ZERO,
			contentOffsetOnPress,
		)
	}
}