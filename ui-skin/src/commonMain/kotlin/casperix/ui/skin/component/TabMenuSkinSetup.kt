package casperix.ui.skin.component

import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.tab.SidedTabMenuSkin
import casperix.ui.component.tab.TabMenuSkin
import casperix.ui.engine.Supplier
import casperix.ui.layout.Align
import casperix.ui.type.LayoutSide

class TabMenuSkinSetup(supplier: Supplier, storage: HierarchicalStorage, val buttonsSkinSetup: ButtonsSkinSetup, val textSkinSetup: TextSkinSetup) {
	val tabMenuSkin = TabMenuSkin(
		createSidedTabMenuSkin(supplier, LayoutSide.TOP),
		createSidedTabMenuSkin(supplier, LayoutSide.BOTTOM),
		createSidedTabMenuSkin(supplier, LayoutSide.LEFT),
		createSidedTabMenuSkin(supplier, LayoutSide.RIGHT),
		Align.CENTER,
		2.0,
	)

	init {
		storage.set(tabMenuSkin)
	}

	private fun createSidedTabMenuSkin(supplier: Supplier, side: LayoutSide): SidedTabMenuSkin {
		return SidedTabMenuSkin(buttonsSkinSetup.createTabToggle(supplier, side), null, textSkinSetup.headerBack, null, null)
	}
}