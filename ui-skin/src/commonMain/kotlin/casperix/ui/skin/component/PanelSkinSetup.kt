package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.frame.FrameSkin
import casperix.ui.component.panel.PanelSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier

class PanelSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val panelBack = supplier.image9Slice(FileReference.classpath("graphics/panel/normal.png"), 2.0, -1.0)
	val panelGap = SideIndents(0)
	val panelBorder = SideIndents(0)

	val panelSkin = PanelSkin(panelBack, panelGap, panelBorder)
	val frameSkin = FrameSkin(
		textSkinSetup.titleText,
		40.0,
		textSkinSetup.headerBack,
		panelBack
	)

	init {
		storage.set(frameSkin)
		storage.set(panelSkin)
	}

}