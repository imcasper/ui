package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.math.color.Color
import casperix.math.vector.Vector2d
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.text.TextShadowSettings
import casperix.ui.component.text.TextSkin
import casperix.ui.component.texteditor.TextCursorSkin
import casperix.ui.component.texteditor.TextEditorSkin
import casperix.ui.component.texteditor.TextInputSkin
import casperix.ui.engine.Supplier

class TextInputSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val textCursorSkin = TextCursorSkin(
		0.4,
		supplier.image9Slice(FileReference.classpath("graphics/texteditor/cursor.png"), 3.0, -5.0),
		supplier.image9Slice(FileReference.classpath("graphics/texteditor/cursorInverted.png"), 3.0, -5.0),
	)

	val inputTextSkin = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, color = Color.WHITE.setAlpha(0.8), shadow = TextShadowSettings(Vector2d(0.0, 2.0), Color.BLACK.setAlpha(1.0)))

	val inputSkin = TextInputSkin(
		inputTextSkin,
		textCursorSkin,
		supplier.image9Slice(FileReference.classpath("graphics/texteditor/normal.png"), 3.0),
		supplier.image9Slice(FileReference.classpath("graphics/texteditor/active.png"), 3.0),
	)

	val textEditorSkin = TextEditorSkin(
		inputSkin,
		TextSkin(textSkinSetup.defaultFont, textSkinSetup.textGap, Color.SILVER),
	)

	init {
		storage.set(inputSkin)
		storage.set(textEditorSkin)
	}
}