package casperix.ui.skin.component

import casperix.file.FileReference
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.progress.ProgressBarSkin
import casperix.ui.component.progress.SidedProgressBarSkin
import casperix.ui.core.SideIndents
import casperix.ui.core.UINode
import casperix.ui.engine.Supplier

class ProgressBarSkinSetup(supplier: Supplier, storage:HierarchicalStorage) {
	val progressBar = supplier.imageStretch(FileReference.classpath("graphics/progress/bar.png"))
	val progressBackHorizontal = supplier.image9Slice(FileReference.classpath("graphics/progress/backHorizontal.png"), SideIndents(2, 6), SideIndents.ZERO)
	val progressBackVertical = supplier.image9Slice(FileReference.classpath("graphics/progress/backVertical.png"), SideIndents(6, 2), SideIndents.ZERO)
	val progressBarSkin = ProgressBarSkin(SidedProgressBarSkin(progressBackHorizontal, progressBar), SidedProgressBarSkin(progressBackVertical, progressBar), SideIndents(2))

	init {
		storage.set(progressBarSkin)
	}
}