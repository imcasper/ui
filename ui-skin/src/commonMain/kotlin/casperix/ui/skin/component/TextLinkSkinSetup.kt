package casperix.ui.skin.component

import casperix.math.color.Color
import casperix.math.vector.Vector2d
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.LinkViewSkin
import casperix.ui.component.text.TextShadowSettings
import casperix.ui.component.text.TextSkin
import casperix.ui.engine.Supplier

class TextLinkSkinSetup(supplier: Supplier, storage: HierarchicalStorage, textSkinSetup: TextSkinSetup) {
	val disabledLink = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, underline = false, color = Color(0, 0, 255, 255))
	val normalLink = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, underline = false, color = Color(0, 0, 255, 255), shadow = TextShadowSettings(Vector2d(0.0, -1.0), Color.WHITE.setAlpha(0.5)))
	val focusedLink = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, underline = true, color = Color(64, 120, 196, 255), shadow = TextShadowSettings(Vector2d(0.0, -1.0), Color.WHITE.setAlpha(0.5)))
	val pressedLink = TextSkin(textSkinSetup.buttonFont, textSkinSetup.textGap, underline = true, color = Color(64, 120, 196, 255), shadow = TextShadowSettings(Vector2d(0.0, -1.0), Color.WHITE.setAlpha(0.5)))
	val textLinkSkin = LinkViewSkin(
		disabledLink,
		normalLink,
		focusedLink,
		pressedLink,
	)

	init {
		storage.set(textLinkSkin)
	}
}