package casperix.ui.skin.component

import casperix.math.vector.Vector2i
import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.button.ButtonDrawType
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier
import casperix.ui.skin.component.ButtonsSkinSetup

class MobileButtonSkinSetup(supplier: Supplier, storage: HierarchicalStorage, buttonsSkinSetup: ButtonsSkinSetup) {
	val hitBorder = SideIndents(1, 2, 0, -1)
	val defaultButtonSkin = buttonsSkinSetup.create9SliceButtonSkin(supplier, "graphics/button2/", 28.0, -8.0, textOffsetOnPress = Vector2i(0, 4), drawType = ButtonDrawType.SIMPLIFY, hitBorder = hitBorder).copy(allowClipping = false)
	val redButtonSkin = buttonsSkinSetup.create9SliceButtonSkin(supplier, "graphics/button2/red/", 28.0, -8.0, textOffsetOnPress = Vector2i(0, 4), drawType = ButtonDrawType.SIMPLIFY, hitBorder = hitBorder).copy(allowClipping = false)

	val togglHitBorder = SideIndents(1, 2, 0, -5)
	val toggleSkin = ToggleSkin(
		buttonsSkinSetup.create9SliceButtonSkin(supplier, "graphics/button2/toggle/on/", 28.0, -8.0, textOffsetOnPress = Vector2i(0, -4), drawType = ButtonDrawType.SIMPLIFY, hitBorder = togglHitBorder).copy(allowClipping = false),
		buttonsSkinSetup.create9SliceButtonSkin(supplier, "graphics/button2/toggle/off/", 28.0, -8.0, textOffsetOnPress = Vector2i(0, 4), drawType = ButtonDrawType.SIMPLIFY, hitBorder = togglHitBorder).copy(allowClipping = false),
	)

	init {
		storage.set(defaultButtonSkin)
		storage.set(redButtonSkin, "red")
		storage.set(toggleSkin)
	}
}