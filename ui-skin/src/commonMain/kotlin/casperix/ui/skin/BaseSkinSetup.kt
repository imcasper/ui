package casperix.ui.skin

import casperix.ui.attribute.HierarchicalStorage
import casperix.ui.component.texteditor.LongKeyRepeaterSettings
import casperix.ui.engine.Supplier
import casperix.ui.skin.component.*
import casperix.ui.source.FontSource

class BaseSkinSetup(supplier: Supplier, storage:HierarchicalStorage, val defaultFontSize: Int = 12, customFontSource: FontSource?) {
	val textSkinSetup = TextSkinSetup(supplier, storage, defaultFontSize, customFontSource)
	val panelSkinSetup = PanelSkinSetup(supplier, storage, textSkinSetup)
	val scrollerSkinSetup = ScrollerSkinSetup(supplier, storage, panelSkinSetup)
	val textInputSkinSetup = TextInputSkinSetup(supplier, storage, textSkinSetup)
	val consoleSkinSetup = ConsoleSkinSetup(supplier, storage, textSkinSetup, scrollerSkinSetup, textInputSkinSetup)
	val buttonsSetup = ButtonsSkinSetup(supplier, storage, textSkinSetup)
	val selectableSkinSetup = SelectableSkinSetup(supplier, storage, textSkinSetup, buttonsSetup)
	val comboBoxSkinSetup = ComboBoxSkinSetup(supplier, storage, selectableSkinSetup)
	val progressBarSetup = ProgressBarSkinSetup(supplier, storage)
	val treeSetup = TreeSkinSetup(supplier, storage, textSkinSetup)
	val tabMenuSkinSetup = TabMenuSkinSetup(supplier, storage, buttonsSetup, textSkinSetup)
	val textLinkSkinSetup = TextLinkSkinSetup(supplier, storage, textSkinSetup)
	val debugSkinSetup = DebugSkinSetup(supplier, storage, textSkinSetup)

	init {
		storage.set(LongKeyRepeaterSettings(0.025, 0.250))
	}
}