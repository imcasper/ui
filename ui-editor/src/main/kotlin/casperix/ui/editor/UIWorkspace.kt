package casperix.ui.editor

import casperix.math.color.Color4d
import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.ui.core.SideIndents
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.editor.model.NodeModel
import casperix.ui.graphic.ShapeBuilder
import casperix.ui.graphic.ShapeGraphic
import casperix.ui.graphic.ShapeStyle
import casperix.ui.input.TouchCatcherFactory

class NodeModelTag(node: UINode, val model: NodeModel) : UIComponent(node)

class UIWorkspace(val editor: EditorModel) : UIComponent(UINode()) {
	private val top = UINode()
	private val back = UINode()
	private val modelContainer = UINode()
	private val selectionRender = SelectionRender(editor, top, modelContainer)

	init {
		node.placement.gap = SideIndents(16)
		node += back
		node += modelContainer
		node += top

		top.touchFilterBuilder = TouchCatcherFactory::always

		back.events.onSize.then {
			back.graphic = ShapeGraphic(ShapeBuilder.rectangle(Vector2d.ZERO, back.placement.size, ShapeStyle(Color4d(0.4, 0.4, 0.4, 1.0))))
		}

		top.inputs.onTouchDown.then(components) { event ->
			click(event.position)
		}

		editor.cmdChanged.then { rebuildView() }
		editor.cmdAdd.then { rebuildView() }
		editor.cmdRemove.then { rebuildView() }
		editor.onRoot.then { rebuildView() }
	}

	private fun click(position: Vector2d) {
		val tag = NodeModelTagSearch.searchByPosition(modelContainer, position)
		editor.cmdSelect.value = tag?.model
	}

	private fun dropSelect() {
		editor.cmdSelect.value = null
	}

	private fun rebuildView() {
		val root = editor.onRoot.value

		modelContainer.children.clear()
		modelContainer += UIFactory.buildUI(root)
	}
}

