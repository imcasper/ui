package casperix.ui.editor

import casperix.ui.editor.model.NodeModel

class EditorController(val editor: EditorModel) {
	init {
		editor.cmdAdd.then { item ->
			editor.actualTarget.children += item
		}
		editor.cmdRemove.then { item ->
			remove(item)
			updateSelected()
		}
	}

	private fun updateSelected() {
		val item = editor.cmdSelect.value ?: return
		val root = editor.onRoot.value

		if (!deepSearch(root, item)) {
			editor.cmdSelect.value = null
		}
	}

	private fun deepSearch(root: NodeModel, item: NodeModel): Boolean {
		if (root == item) return true
		root.children.forEach { child->
			if (deepSearch(child, item)) return true
		}
		return false
	}

	private fun remove(item: NodeModel):Boolean {
		val parent = editor.getParent(editor.actualTarget) ?: return false
		parent.children.remove(item)
		return true
	}



}