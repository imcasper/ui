package casperix.ui.editor

import casperix.gdx.app.GdxApplicationConfig
import casperix.gdx.app.GdxDesktopApplicationLauncher
import casperix.gdx.app.GdxDesktopWindowConfig

fun main(args: Array<String>) {
	GdxDesktopApplicationLauncher(
		GdxApplicationConfig(
			"UI Editor", "icon.png",
			desktop = GdxDesktopWindowConfig()
		)
	) {
		App(it)
	}
}

