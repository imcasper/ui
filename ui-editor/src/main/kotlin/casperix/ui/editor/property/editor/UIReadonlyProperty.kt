package casperix.ui.editor.property.editor

import casperix.ui.component.text.TextView
import casperix.ui.component.timer.TimerLogic
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout

class UIReadonlyProperty(getter: () -> String?) : UIComponent(UINode(layout = Layout.HORIZONTAL)) {
	val label = TextView("" )

	init {
		node += label

		TimerLogic.timeInterval(node, 1.0) {
			label.text = getter.invoke() ?: "<empty>"
		}
	}
}