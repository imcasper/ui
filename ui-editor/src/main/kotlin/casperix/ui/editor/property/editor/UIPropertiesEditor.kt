package casperix.ui.editor.property.editor

import casperix.misc.toPrecision
import casperix.ui.component.combobox.ComboBoxView
import casperix.ui.component.panel.PanelView
import casperix.ui.component.text.TextView
import casperix.ui.component.texteditor.TextEditor
import casperix.ui.core.*
import casperix.ui.editor.EditorModel
import casperix.ui.editor.UIStyle
import casperix.ui.editor.model.NodeModel
import casperix.ui.editor.property.*
import casperix.ui.editor.property.custom.CommonPropertyBuilder
import casperix.ui.layout.Layout


class UIPropertiesEditor(val editor: EditorModel) : UIComponent(UINode(layout = Layout.VERTICAL)) {
	private val headerText = TextView("")
	private val propertyContainer = UINode(layout = Layout.VERTICAL)

	init {
		PanelView(node)
		node.placement.sizeMode = SizeMode(UIStyle.basicPanelWidth, MaxChildrenOrViewDimension)
		node += UINode().apply {
			this += headerText
		}
		node += propertyContainer

		editor.cmdSelect.then {
			setup(it)
		}
	}

	private fun setup(selected: NodeModel?) {
		propertyContainer.children.clear()

		if (selected == null) {
			headerText.text = "Select any element..."

		} else {
			headerText.text = "Property editor"

			CommonPropertyBuilder.setup(selected).forEach {
				propertyContainer += createPropertyController(selected, it)
			}
		}
	}

	private fun createPropertyController(selected: NodeModel, property: Property): UINode {
		return when (property) {
			is ReadonlyProperty<*> -> {
				addCustom(property.label, UIReadonlyProperty {
					property.getCurrentName
				})
			}

			is EnumProperty<*> -> {
				val box = ComboBoxView(UIStyle.cellSize, property.getter(), property.variants, { value ->
					(property as EnumProperty<Any>).getName(value)
				}, { value ->
					(property as EnumProperty<Any>).setter(value)
					onRefresh(selected)
				})
				addCustom(property.label, box)
			}

			is DoubleProperty -> {
				val firstValue = property.getter()
				addCustom(property.label, TextEditor(null, firstValue.toPrecision(0), "", UINode()).apply {
					inputComponent.node.placement.sizeMode = SizeMode.const(UIStyle.cellSize)
					logic.allowedSymbols = ".0123456789"
					logic.onText.then {
						property.setter(it.toDoubleOrNull() ?: firstValue)
						onRefresh(selected)
					}
				})
			}

			is TextProperty -> {
				addCustom(property.label, TextEditor(null, property.getter(), "<unnamed>", UINode()).apply {
					inputComponent.node.placement.sizeMode = SizeMode.const(UIStyle.cellSize)
					logic.onText.then {
						property.setter(it)
						onRefresh(selected)
					}
				})
			}

			else -> {
				TextView("Unsupported property").node
			}
		}
	}

	private fun onRefresh(selected:NodeModel) {
		editor.cmdChanged.set(selected)
	}

	private fun addCustom(label: String, component: UIComponent): UINode {
		val cell = UINode(layout = Layout.HORIZONTAL)
		cell += UINode(UIStyle.propertyLabelSize).apply {
			this += TextView(label)
		}
		cell += component
		return cell
	}

}


