package casperix.ui.editor.property

interface Property {
	val label: String
}

class EnumProperty<Value : Any>(override val label: String, val getName: (Value) -> String, val variants: List<Value>, val getter: () -> Value, val setter: (Value) -> Unit) : Property

class ReadonlyProperty<Value : Any>(override val label: String, val getName: (Value) -> String, val getter: () -> Value) : Property {
	val getCurrentName: String get()  {
		val value = getter()
		return if (value == "") {
			"<undefined>"
		} else {
			getName(value)
		}
	}
}

class DoubleProperty(override val label: String, val getter: () -> Double, val setter: (Double) -> Unit) : Property

class TextProperty(override val label: String, val getter: () -> String, val setter: (String) -> Unit) : Property