package casperix.ui.editor.serialize

import casperix.ui.editor.EditorModel
import casperix.ui.editor.model.*
import com.badlogic.gdx.Gdx
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.SerializersModuleBuilder
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass

class Serializer(val editor: EditorModel) {
	fun SerializersModuleBuilder.registerComplexTypes() {
		polymorphic(NodeComponent::class) {
			subclass(PanelComponent::class)
			subclass(TextComponent::class)
			subclass(ButtonComponent::class)
		}
	}

	private val module = SerializersModule {
		registerComplexTypes()
	}

	val JSON = Json { serializersModule = module; allowStructuredMapKeys = true; prettyPrint = true }

	init {
		editor.cmdSave.then {
			save()
		}
		editor.cmdLoad.then {
			load()
		}
	}

	private fun save() {
		try {
			val json = JSON.encodeToString(editor.onRoot.value)
			Gdx.files.external("autosave.gui").writeString(json, false)
		} catch (error: Throwable) {
			error.printStackTrace()
		}
	}

	private fun load() {
		try {
			val json = Gdx.files.external("autosave.gui").readString()
			val entry = JSON.decodeFromString<NodeModel>(json)
			editor.onRoot.value = entry
		} catch (error: Throwable) {
			error.printStackTrace()
		}
	}

}
