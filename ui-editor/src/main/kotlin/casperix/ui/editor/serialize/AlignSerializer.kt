package casperix.ui.editor.serialize

import casperix.ui.layout.Align
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlin.math.roundToInt

object AlignSerializer : KSerializer<Align> {
	override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("DimensionMode", PrimitiveKind.STRING)

	override fun serialize(encoder: Encoder, value: Align) {
		val output = when (value) {
			Align.MIN -> "min"
			Align.CENTER -> "center"
			Align.MAX -> "max"
			else -> "f_" + (value.factor * 100.0).roundToInt()
		}
		encoder.encodeString(output)
	}

	override fun deserialize(decoder: Decoder): Align {
		val input = decoder.decodeString()
		return when (input) {
			"min" -> Align.MIN
			"max" -> Align.MAX
			"center" -> Align.CENTER
			else -> {
				val value = input.removePrefix("f_").toIntOrNull() ?: throw Error("Unsupported dimension")
				Align(value / 100.0)
			}
		}
	}
}