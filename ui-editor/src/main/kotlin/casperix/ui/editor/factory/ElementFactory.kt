package casperix.ui.editor.factory

import casperix.ui.editor.model.NodeModel

class ElementFactory(val name: String, val builder: () -> NodeModel)