package casperix.ui.editor.factory

import casperix.ui.core.ConstDimension
import casperix.ui.editor.model.ButtonComponent
import casperix.ui.editor.model.NodeModel
import casperix.ui.editor.model.PanelComponent
import casperix.ui.editor.model.TextComponent

object ElementFactoryStorage {
	val variants = listOf(
		ElementFactory("button") {
			NodeModel().apply {
				component = ButtonComponent("test")
				sizeModeWidth = ConstDimension(90.0)
				sizeModeHeight = ConstDimension(30.0)
			}
		},
		ElementFactory("panel") {
			NodeModel().apply {
				component = PanelComponent
			}
		},
		ElementFactory("text") {
			NodeModel().apply {
				component = TextComponent("test")
			}
		},
	)
}