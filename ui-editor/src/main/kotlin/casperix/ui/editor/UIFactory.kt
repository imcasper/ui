package casperix.ui.editor

import casperix.ui.component.button.ButtonView
import casperix.ui.component.button.ButtonLogic
import casperix.ui.component.panel.PanelView
import casperix.ui.component.text.TextView
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.editor.model.*
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide

object UIFactory {
	fun buildUI(model: NodeModel): UINode {
		val node = UINode()
		NodeModelTag(node, model)

		node.layout = when (model.layout) {
			LayoutMode.NOTHING -> null
			LayoutMode.SCREEN -> Layout.SCREEN
			LayoutMode.VERTICAL -> Layout.VERTICAL
			LayoutMode.HORIZONTAL -> Layout.HORIZONTAL
			LayoutMode.DIVIDE_LEFT -> DividerLayout(LayoutSide.LEFT)
			LayoutMode.DIVIDE_RIGHT -> DividerLayout(LayoutSide.RIGHT)
			LayoutMode.DIVIDE_TOP -> DividerLayout(LayoutSide.TOP)
			LayoutMode.DIVIDE_BOTTOM -> DividerLayout(LayoutSide.BOTTOM)
		}

		model.component?.let { component ->
			when(component) {
				is PanelComponent -> PanelView(node)
				is TextComponent -> TextView(component.label, true, false, node)
				is ButtonComponent -> ButtonView(ButtonLogic(node)).apply {
					addLabel(component.label)
				}
			}
		}

		node.placement.align = LayoutAlign(model.alignHorizontal, model.alignVertical)
		node.placement.sizeMode = SizeMode(model.sizeModeWidth, model.sizeModeHeight)

		 model.children.forEach { childModel ->
			 node += buildUI(childModel)
		}

		return node
	}
}