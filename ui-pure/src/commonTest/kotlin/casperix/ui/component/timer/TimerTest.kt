package casperix.ui.component.timer

import casperix.misc.toPrecision
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue

class TimerTest {

	private fun assertInInterval(expected: Double, actual: Double, maxDelta: Double, message: String? = null) {
		val isGood = (expected - maxDelta < actual && actual < expected + maxDelta)
		assertTrue(isGood, "Expected ${expected.toPrecision(2)}, but actual is ${actual.toPrecision(2)}" + (message ?: ""))

	}

	@Test
	fun testInvalidArgument() {
		assertFails {
			FrameStepCondition(0u)
		}
		assertFails {
			TimeStepCondition(0.0)
		}
		assertFails {
			TimeStepCondition(-12.0)
		}
		assertFails {
			TimeStepCondition(Double.NaN)
		}
	}

	@Test
	fun testAfterTime() {
		val skipTime = 5.0
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.afterTime(scene.root, skipTime) {
				assertInInterval(skipTime, scene.time, TestTimerScene.MAX_TICK)
				testCounter++
			}
			assertEquals(0, testCounter)
		}, {
			assertEquals(1, testCounter)
		})
	}

	@Test
	fun testAfterFrame() {
		val skipFrame = 5u
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.afterFrame(scene.root, skipFrame) {
				assertEquals(skipFrame.toInt(), scene.frame + 1)
				testCounter++
			}
			assertEquals(0, testCounter)
		}, {
			assertEquals(1, testCounter)
		})
	}

	@Test
	fun testDispose1() {
		val skipFrame = 5u
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.afterFrame(scene.root, skipFrame) {
				assertEquals(skipFrame.toInt(), scene.frame + 1)
				testCounter++
			}.dispose()
			assertEquals(0, testCounter)
		}, {
			assertEquals(0, testCounter)
		})
	}

	@Test
	fun testDispose2() {
		val skipFrame = 10u
		val breakFrame = 5
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.frameInterval(scene.root, skipFrame) {
				if (++testCounter == breakFrame) {
					it.dispose()
				}
			}
			assertEquals(0, testCounter)
		}, {
			assertEquals(breakFrame, testCounter)
		})
	}


	@Test
	fun testFrameInterval() {
		val frameInterval = 10
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.frameInterval(scene.root, frameInterval.toUInt()) {
				assertEquals(frameInterval * testCounter, scene.frame)
				testCounter++
			}
		}, {

		})
	}

	@Test
	fun testTimeInterval() {
		val timeInterval = 10.0
		var testCounter = 0

		TestTimerScene.run({ scene ->
			TimerLogic.timeInterval(scene.root, timeInterval) {
				assertInInterval(timeInterval * testCounter, scene.time, TestTimerScene.MAX_TICK, "tick: ${scene.tick}")
				testCounter++
			}
		}, {

		})
	}
}
