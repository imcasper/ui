package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.color.Color4d
import casperix.ui.core.SideIndents
import casperix.ui.engine.Bitmap
import casperix.ui.engine.Drawer
import kotlin.math.min
import kotlin.math.roundToInt

data class BitmapGraphic(
	val bitmap: Bitmap,
	val scaling: Scaling = ScalingNone(),
	val border: SideIndents = SideIndents.ZERO,
	val color: Color4d = Color4d.ONE,
	val flipX: Boolean = false,
	val flipY: Boolean = false,
	val turn90CW: Int = 0,
) : Graphic {

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		if (!position.isFinite()) throw Error("Invalid position: $position")
		if (!size.isFinite()) throw Error("Invalid size: $size")

		if (scaling is Scaling9Slice) {
			val slices = scaling.slices
			val targetPosition = (border.leftTop + position)
			val targetSize = size - border.size

			val targetWidth = targetSize.x
			val targetHeight = targetSize.y
			val sourceWidth = bitmap.size.x.toDouble()
			val sourceHeight = bitmap.size.y.toDouble()


			val sourceX = getPositionSequence(sourceWidth, slices.left, slices.right)
			val sourceY = getPositionSequence(sourceHeight, slices.top, slices.bottom)

			val targetX = getPositionSequence(targetWidth, slices.left, slices.right)
			val targetY = getPositionSequence(targetHeight, slices.top, slices.bottom)

			for (u in 0..2) {
				for (v in 0..2) {
					val sx1 = sourceX[u]
					val sx2 = sourceX[u + 1]
					val sy1 = sourceY[v]
					val sy2 = sourceY[v + 1]
					val tx1 = targetX[u]
					val tx2 = targetX[u + 1]
					val ty1 = targetY[v]
					val ty2 = targetY[v + 1]

					val partTargetMin = Vector2d(tx1, ty1)
					val partTargetMax = Vector2d(tx2, ty2)
					val partTargetSize = partTargetMax - partTargetMin

					val partSourceMin = Vector2d(sx1, sy1).roundToVector2i()
					val partSourceMax = Vector2d(sx2, sy2).roundToVector2i()
					val partSourceSize = partSourceMax - partSourceMin

					if (partTargetSize.volume() > 0) {
						drawer.drawBitmap(bitmap, targetPosition + partTargetMin, partTargetSize, partSourceMin, partSourceSize, color = color)
					}
				}
			}
		} else {
			var imageOffset = Vector2d.ZERO

			val imageSize = if (scaling is ScalingUniform) {
				bitmap.size.toVector2d() * scaling.scale
			} else if (scaling is ScalingNone) {
				bitmap.size.toVector2d()
			} else if (scaling is ScalingAdaptive) {
				val defaultSize = bitmap.size.toVector2d()
				val scaleX = (size.x - border.size.x) / defaultSize.x
				val scaleY = (size.y - border.size.y) / defaultSize.y
				if (scaleX > scaleY) {
					imageOffset += Vector2d(defaultSize.x * (scaleX - scaleY) / 2.0, 0.0)
				} else {
					imageOffset += Vector2d(0.0, defaultSize.y * (scaleY - scaleX) / 2.0)
				}
				defaultSize * min(scaleX, scaleY)
			} else {
				size - border.size
			}

			if (!imageSize.greater(Vector2d.ZERO)) return

			val shapeSize = if (turn90CW % 2 == 0) imageSize else Vector2d(imageSize.y, imageSize.x)
			val rotate = turn90CW * 90.0

			drawer.drawTransformedBitmap(bitmap, position + border.leftTop + imageSize / 2.0 + imageOffset, shapeSize, color = color, flipX = flipX, flipY = flipY, rotateDegreeCW = rotate)
		}
	}

	private fun getPositionSequence(width: Double, leftIndent: Double, rightIndent: Double): List<Double> {
		if (leftIndent + rightIndent > width) {
			return listOf(
				0.0,
				(width / 3.0).roundToInt().toDouble(),
				(width * 2.0 / 3.0).roundToInt().toDouble(),
				(width)
			)
		}

		return listOf(
			0.0,
			(leftIndent).roundToInt().toDouble(),
			(width - rightIndent).roundToInt().toDouble(),
			width,
		)
	}
}