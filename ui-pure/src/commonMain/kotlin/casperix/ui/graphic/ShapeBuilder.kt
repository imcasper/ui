package casperix.ui.graphic

import casperix.math.axis_aligned.Box2d
import casperix.math.color.Color4d
import casperix.math.geometry.*
import casperix.math.vector.Vector2d
import casperix.math.vector.rotateBy90
import casperix.math.vector.toQuad
import casperix.misc.clamp
import casperix.ui.engine.Shape
import casperix.ui.engine.ShapeMaterial
import casperix.ui.engine.ShapeSegment
import kotlin.math.*

object ShapeBuilder {
	fun rectangle(position: Vector2d, size: Vector2d, style: ShapeStyle): Shape {
		return quad(Box2d.byDimension(position, size).toQuad(), style)
	}

	fun rectangle(box: Box2d, style: ShapeStyle): Shape {
		return quad(box.toQuad(), style)
	}

	fun triangle(triangle: Triangle2d, style: ShapeStyle): Shape {
		var result = Shape(emptyList())
		if (style.hasBody) {
			result += rawTriangle(triangle, style.bodyColor)
		}
		if (style.borderThick != 0.0) {
			result += rawBorder(Line2d(triangle.v0, triangle.v1), style)
			result += rawBorder(Line2d(triangle.v1, triangle.v2), style)
			result += rawBorder(Line2d(triangle.v2, triangle.v0), style)
		}
		return result
	}

	fun quad(quad: Quad2d, style: ShapeStyle): Shape {
		var result = Shape(emptyList())
		if (style.hasBody) {
			result += rawQuad(quad, style.bodyColor)
		}
		if (style.hasBorder) {
			result += rawBorder(Line2d(quad.v0, quad.v1), style)
			result += rawBorder(Line2d(quad.v1, quad.v2), style)
			result += rawBorder(Line2d(quad.v2, quad.v3), style)
			result += rawBorder(Line2d(quad.v3, quad.v0), style)
		}
		return result
	}

	fun ellipse(color: Color4d, center: Vector2d, xRange: Double, yRange: Double, style: ShapeStyle): Shape {
		val sectors = rawSectors(center, xRange, yRange)
		var result = Shape(emptyList())
		if (style.hasBody) {
			result += Shape(listOf(ShapeSegment(ShapeMaterial(color), sectors)))
		}
		if (style.hasBorder) {
			sectors.forEach {
				result += rawBorder(Line2d(it.v1, it.v2), style)
			}
		}
		return result
	}

	private fun rawTriangle(triangle: Triangle2d, color: Color4d): Shape {
		return Shape(listOf(ShapeSegment(ShapeMaterial(color), listOf(triangle))))
	}

	private fun rawQuad(quad: Quad2d, color: Color4d): Shape {
		return Shape(listOf(ShapeSegment(ShapeMaterial(color), listOf(quad.getFace(0), quad.getFace(1)))))
	}

	private fun rawBorder(line: Line2d, style: ShapeStyle): Shape {
		return rawLine(line, style.borderColor, style.borderThick)
	}

	private fun rawLine(line: Line2d, color: Color4d, thick: Double): Shape {
		val side = line.direction().rotateBy90().normalize() * thick * 0.5
		return rawQuad(Quad2d(line.v0 + side, line.v0 - side, line.v1 - side, line.v1 + side), color)
	}

	private fun rawSectors(center: Vector2d, xRange: Double, yRange: Double): List<Triangle<Vector2d>> {
		val resolution = (sqrt(xRange + yRange) * 2.0).roundToInt().clamp(3, 100)

		return (0 until resolution).map {
			val start = PI * 2.0 * it / resolution
			val finish = PI * 2.0 * (it + 1) / resolution

			val v0 = center
			val v1 = center + Vector2d(cos(start) * xRange, sin(start) * yRange)
			val v2 = center + Vector2d(cos(finish) * xRange, sin(finish) * yRange)
			Triangle(v0, v1, v2)
		}
	}
}