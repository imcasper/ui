package casperix.ui.graphic

import casperix.math.vector.Vector2d
import casperix.math.geometry.Quad2d
import casperix.math.color.Color4d
import casperix.ui.engine.Drawer

/**
 * 	@see ShapeGraphic
 */
@Deprecated(message = "")
data class QuadGraphic(
	val color: Color4d,
	val shape: Quad2d,
) : Graphic {
	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val quad = shape.convert { it + position }
		drawer.drawQuad(quad, color)
	}

}