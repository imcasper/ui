package casperix.ui.graphic

import casperix.ui.core.SideIndents

sealed class Scaling

class ScalingNone : Scaling()
class ScalingAdaptive : Scaling()
class ScalingStretch : Scaling()
class ScalingUniform(val scale: Double) : Scaling()
class Scaling9Slice(val slices: SideIndents) : Scaling() {
	constructor(indents: Double) : this(SideIndents(indents))
}
