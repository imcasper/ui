package casperix.ui.input

import casperix.input.PointerEvent
import casperix.math.intersection.intersectionPointWithTriangle
import casperix.ui.core.UINode
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.ColoredGraphic
import casperix.ui.graphic.ShapeGraphic
import casperix.ui.graphic.TextGraphic

/**
 * 	Improvement experimental touch catcher
 */
object SmartTouchCatcher {
	//	TODO check alpha value
	fun inside(node: UINode, event: PointerEvent): TouchCatcher {
		val graphic = node.graphic ?: return TouchCatcher(false, false)

		val absolutePosition = node.absolutePosition
		when (graphic) {
			is BitmapGraphic -> return TouchCatcherFactory.inside(node, event)

			is ColoredGraphic -> return TouchCatcherFactory.inside(node, event)
			is TextGraphic -> return TouchCatcherFactory.inside(node, event)
			is ShapeGraphic -> {
				graphic.shape.segments.forEach { segment ->
					segment.triangles.forEach {
						if (intersectionPointWithTriangle(event.position - absolutePosition, it)) return TouchCatcher(true, true)
					}
				}
			}
		}
		return TouchCatcher(false, false)
	}
}