package casperix.ui.input

import casperix.ui.core.UINode

class ContinuousEventController {
	private var lastList = emptySet<UINode>()
	private var currentList = emptySet<UINode>()

	fun items():Collection<UINode> {
		return currentList
	}

	fun action(node:UINode?, onOut:(UINode)->Unit, onOver:(UINode)->Unit) {
		lastList = currentList
		currentList = collectAll(node)

		val outList = (lastList - currentList)
		val overList = (currentList - lastList)

		outList.forEach {
			onOut(it)
		}

		overList.forEach {
			onOver(it)
		}
	}

	private fun collectAll(node: UINode?):Set<UINode> {
		val nodes = hashSetOf<UINode>()

		var next:UINode? = node
		while (next != null) {
			nodes.add(next)
			next = next.parent
		}
		return nodes
	}
}