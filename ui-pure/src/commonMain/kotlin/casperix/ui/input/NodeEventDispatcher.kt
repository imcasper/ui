package casperix.ui.input

import casperix.app.window.Cursor
import casperix.misc.Disposable
import casperix.math.vector.Vector2d
import casperix.input.*
import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.core.*
import casperix.ui.graphic.Graphic
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign


@Deprecated(message = "Use TouchFocused")
class FocusOut(val node: UINode, position: Vector2d) : PointerEvent(position)
@Deprecated(message = "Use TouchFocused")
class FocusOver(val node: UINode, position: Vector2d) : PointerEvent(position)

@Deprecated(message = "Use MouseFocused")
class MouseLeave(val node: UINode, position: Vector2d) : PointerEvent(position)
@Deprecated(message = "Use MouseFocused")
class MouseEnter(val node: UINode, position: Vector2d) : PointerEvent(position)

class TouchFocused(val node: UINode, position: Vector2d) : PointerEvent(position)
class MouseFocused(val node: UINode, position: Vector2d) : PointerEvent(position)

/**
 * 	Сигнал мыши, клавиатуры или тачпада генерит событие.
 * 	Событие проходит иерархию элементов.
 * 	@see UINode
 *
 * 	Пойманное событие генерит "интерфейсное событие", которое идет вниз до корня иерархии
 * 	@see UINode.touchFilterBuilder
 * 	@see TouchCatcher
 */
class NodeInputDispatcher : InputDispatcher by DefaultInputDispatcher() {
	val onTouchFocused = StorageSignal<TouchFocused?>(null)
	val onMouseFocused = StorageSignal<MouseFocused?>(null)

	fun capturePointerEvent(components: MutableCollection<Disposable>) {
		onMouseMove.then(components) { it.captured = true }
		onMouseWheel.then(components) { it.captured = true }
		onTouchDown.then(components) { it.captured = true }
		onTouchUp.then(components) { it.captured = true }
		onTouchDragged.then(components) { it.captured = true }
	}
}

/**	Experimental feature	*/
data class GlobalAction(val key:String, val source:UINode)

class NodeEventDispatcher {
	val onRoot = Signal<UINode>()
	val onParent = Signal<UINode?>()
	val onAddChild = Signal<UINode>()
	val onRemoveChild = Signal<UINode>()
	val onPosition = Signal<Vector2d>()
	val onChildrenSize = Signal<Vector2d>()
	val onSize = Signal<Vector2d>()

	val onAlign = Signal<LayoutAlign>()
	val onSizeMode = Signal<SizeMode>()
	val onBorder = Signal<SideIndents>()
	val onLayout = Signal<Layout?>()
	val onGraphic = Signal<Graphic?>()

	val propertyChanged = EmptySignal()
	val nextFrame = Signal<Double>()

	val onGlobalAction = Signal<GlobalAction>()
}

object CommonNodeEvent {
	val onCursor = Signal<Cursor>()
}