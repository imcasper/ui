package casperix.ui.input

import casperix.input.PointerEvent
import casperix.math.axis_aligned.Box2d
import casperix.ui.core.UINode

object TouchCatcherFactory {

	fun isClipping(node: UINode, event: PointerEvent): Boolean {
		val absolutePosition = node.absolutePosition
		val absoluteSize = node.placement.size
		if (absoluteSize.x <= 0 || absoluteSize.y <= 0) return true
		return Box2d(absolutePosition, absolutePosition + absoluteSize).isOutside(event.position)
	}

	fun inside(node: UINode, event: PointerEvent): TouchCatcher {
		val absolutePosition = node.absolutePosition
		val absoluteSize = if (node.clippingContent) node.placement.size else node.placement.size.upper(node.layoutExecutor.childrenSize)
		if (absoluteSize.x <= 0 || absoluteSize.y <= 0) return TouchCatcher(false, false)
		val enabled = Box2d(absolutePosition, absolutePosition + absoluteSize).isInside(event.position)
		return TouchCatcher(node.graphic != null && enabled, enabled)
	}

	fun always(node: UINode, event: PointerEvent): TouchCatcher {
		return TouchCatcher(true, true)
	}

	fun never(node: UINode, event: PointerEvent): TouchCatcher {
		return TouchCatcher(false, false)
	}
//
//	fun stopSelf(node: UINode, position: Vector2d): HierarchyFilter {
//		return HierarchyFilter( false, true)
//	}
}