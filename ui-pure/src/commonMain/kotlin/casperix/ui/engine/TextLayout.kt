package casperix.ui.engine

import casperix.math.vector.Vector2d
import casperix.ui.engine.GlyphInfo

class TextLayout(
	val source:String,
	val size: Vector2d,
	val glyphs: List<GlyphInfo>,
)