package casperix.ui.engine

import casperix.file.FileReference
import casperix.map2d.IntMap2D
import casperix.math.axis_aligned.Box2i
import casperix.ui.core.SideIndents
import casperix.ui.font.Font
import casperix.ui.graphic.*
import casperix.ui.source.FontLink
import casperix.ui.source.FontSource

interface Supplier {
	fun fontFromSource(source: FontSource, symbols: String? = null): Font

	fun bitmapFromFile(file: FileReference, smooth: Boolean = true, mipMap: Boolean = true, region: Box2i? = null): Bitmap
	fun bitmapFromPixels(map: IntMap2D, smooth: Boolean = true, mipMap: Boolean = true, region: Box2i? = null): Bitmap
	fun bitmapFromBytes(encoded: ByteArray, smooth: Boolean = true, mipMap: Boolean = true, region: Box2i? = null): Bitmap
	fun bitmapRemove(file: FileReference)
	fun bitmapRemove(map: IntMap2D)
	fun bitmapRemove(encoded: ByteArray)

	fun fontFromFile(file: FileReference, size: Int, symbols: String? = null): Font {
		return fontFromSource(FontLink(file, size), symbols)
	}

	fun image9Slice(file: FileReference, slices: SideIndents, border: SideIndents = SideIndents.ZERO): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file, false), Scaling9Slice(slices), border)
	}

	fun image9Slice(file: FileReference, indents: Double, border: Double = 0.0): BitmapGraphic {
		return image9Slice(file, SideIndents(indents), SideIndents(border))
	}

	fun imageStretch(file: FileReference, border: SideIndents): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingStretch(), border)
	}

	fun imageStretch(file: FileReference, border: Double = 0.0): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingStretch(), SideIndents(border))
	}

	fun imageScaled(file: FileReference, scale: Double, border: SideIndents): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingUniform(scale), border)
	}

	fun imageScaled(file: FileReference, scale: Double, border: Double = 0.0): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingUniform(scale), SideIndents(border))
	}

	fun image(file: FileReference, border: SideIndents): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingNone(), border)
	}

	fun image(file: FileReference, border: Double = 0.0): BitmapGraphic {
		return BitmapGraphic(bitmapFromFile(file), ScalingNone(), SideIndents(border))
	}
}