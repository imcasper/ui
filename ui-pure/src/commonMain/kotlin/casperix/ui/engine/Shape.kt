package casperix.ui.engine

import casperix.math.color.Color4d
import casperix.math.geometry.Triangle2d


data class ShapeMaterial(
	val color: Color4d,
)

typealias ShapeGeometry = List<Triangle2d>

data class ShapeSegment(
	val material: ShapeMaterial,
	val triangles: ShapeGeometry,
)

data class Shape(
	val segments: List<ShapeSegment>,
) {
	operator fun plus(other:Shape):Shape {
		return Shape(this.segments + other.segments)
	}
}