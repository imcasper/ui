package casperix.ui.engine

import casperix.math.axis_aligned.Box2i
import casperix.math.vector.Vector2i
import casperix.math.color.Color
import casperix.ui.source.BitmapSource

interface Bitmap {
	val source: BitmapSource
	val smooth:Boolean
	val region:Box2i

	val size: Vector2i get() = region.dimension

	fun get(position: Vector2i): Color
	fun get(x: Int, y: Int): Color {
		return get(Vector2i(x, y))
	}
}
