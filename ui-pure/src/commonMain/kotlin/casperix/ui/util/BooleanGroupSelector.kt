package casperix.ui.util

import casperix.signals.collection.ObservableCollection
import casperix.misc.DisposableHolder
import casperix.misc.disposeAll
import casperix.signals.concrete.StoragePromise
import casperix.signals.then

class BooleanGroupSelector(val variants: ObservableCollection<StoragePromise<Boolean>>, val behaviour: Behaviour) : DisposableHolder() {
	enum class Behaviour {
		ALWAYS_ONE,
		MAX_ONE,
	}

	init {
		restartObservers()
	}

	private fun restartObservers() {
		components.disposeAll()

		variants.then(components, { restartObservers() }, { restartObservers() })
		variants.forEach { value ->
			value.then(components) {
				update(value, it)
			}
		}
	}

	private fun update(receiver: StoragePromise<Boolean>, on: Boolean) {
		if (on && (behaviour == Behaviour.ALWAYS_ONE || behaviour == Behaviour.MAX_ONE)) {
			variants.forEach { value ->
				if (value != receiver) {
					value.set(false)
				}
			}
		}

		if (!on && (behaviour == Behaviour.ALWAYS_ONE)) {
			val active = variants.firstOrNull { it.value }
			if (active == null) {
				receiver.set(true)
			}
		}
	}
}