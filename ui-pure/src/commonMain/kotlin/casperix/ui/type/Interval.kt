package casperix.ui.type

data class Interval(val min: Double, val max: Double)