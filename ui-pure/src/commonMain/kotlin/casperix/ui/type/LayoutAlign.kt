package casperix.ui.type

import casperix.math.vector.Vector2d
import casperix.ui.layout.Align

data class LayoutAlign(
	val horizontal: Align,
	val vertical: Align
) {

	fun getPosition(parentSize: Vector2d, childSize: Vector2d): Vector2d {
		return Vector2d(
			horizontal.getPosition(parentSize.x, childSize.x),
			vertical.getPosition(parentSize.y, childSize.y)
		)
	}

	companion object {
		val LEFT_TOP = LayoutAlign(Align.MIN, Align.MIN)
		val CENTER_TOP = LayoutAlign(Align.CENTER, Align.MIN)
		val RIGHT_TOP = LayoutAlign(Align.MAX, Align.MIN)

		val LEFT_CENTER = LayoutAlign(Align.MIN, Align.CENTER)
		val CENTER_CENTER = LayoutAlign(Align.CENTER, Align.CENTER)
		val RIGHT_CENTER = LayoutAlign(Align.MAX, Align.CENTER)

		val LEFT_BOTTOM = LayoutAlign(Align.MIN, Align.MAX)
		val CENTER_BOTTOM = LayoutAlign(Align.CENTER, Align.MAX)
		val RIGHT_BOTTOM = LayoutAlign(Align.MAX, Align.MAX)

		val LEFT = LEFT_CENTER
		val RIGHT = RIGHT_CENTER
		val TOP = CENTER_TOP
		val BOTTOM = CENTER_BOTTOM
	}
}