package casperix.ui.attribute

import kotlin.reflect.KClass

class AttributeStorage() {
	private val table = mutableMapOf<KClass<out Any>, MutableMap<String?, Any>>()

	fun <T : Any> set(value: T, name: String? = null) {
		val key: KClass<out T> = value::class
		set(key, value, name)
	}

	fun <T : Any> set(key: KClass<out T>, value: T, name: String? = null) {
		val map = table.getOrPut(key) { mutableMapOf() }

		if (map.put(name, value) != value) {
			dispatchChanged()
		}
	}

	fun <T : Any> unset(key: KClass<out T>, name: String? = null) {
		val map = table.get(key) ?: return

		if (map.remove(name) != null) {
			dispatchChanged()
		}

		if (map.isEmpty()) {
			table.remove(key)
		}
	}

	fun <T : Any> unset(value: T, name: String? = null) {
		val key: KClass<out T> = value::class
		unset(key, name)
	}

	fun <T : Any> get(kClass: KClass<T>, name: String? = null): T? {
		val map = table.get(kClass)
		val item = map?.get(name)
		if (item != null) return item as T

		return null
	}

	fun dispatchChanged() {
	}
}