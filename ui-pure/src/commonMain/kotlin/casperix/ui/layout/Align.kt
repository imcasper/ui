package casperix.ui.layout

import kotlin.math.min

data class Align(val factor: Double) {
	init {
		if (!factor.isFinite() || factor < 0.0 || factor > 1.0) throw Error("Invalid align actual is $factor but expected [0-1]")
	}

	fun getPosition(parentSize: Double, childSize: Double): Double {
		return getPosition(parentSize, childSize, factor)
	}

	override fun toString(): String {
		return when (this) {
			MIN -> "Min"
			CENTER -> "Center"
			MAX -> "Max"
			else -> "Custom($factor)"
		}
	}

	companion object {
		val MIN = Align(0.0)
		val CENTER = Align(0.5)
		val MAX = Align(1.0)

		fun getPosition(parentSize: Double, childSize: Double, align: Double): Double {
			//	if (parentSize < childSize) return 0.0
			return (parentSize - min(childSize, parentSize)) * align
		}
	}
}

