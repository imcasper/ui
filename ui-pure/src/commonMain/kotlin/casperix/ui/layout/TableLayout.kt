package casperix.ui.layout

import casperix.misc.splitByChunk
import casperix.math.vector.Vector2d
import casperix.ui.type.LayoutSide

data class TableCellBound(val preferredSize:Vector2d? = null, val minSize:Vector2d = Vector2d.ZERO, val maxSize:Vector2d = Vector2d(Double.MAX_VALUE))
data class TableLayout(val rowOrientation: Orientation, val rowLength: Int, val cellSize: TableCellBound? = null) : Layout {
	class Stack(val children: List<LayoutTarget>, val bounds: List<Bound>, val maxBound: Bound)

	init {
		if (rowLength < 1) throw Error("Expected positive column amount, but actual is $rowLength")
	}
	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {
		val rows = children.splitByChunk(rowLength).map { createStack(it, viewSize) }
		val columns = List(rowLength) { column -> createColumn(rows.size, column, children, viewSize) }

		val alongRow = rowOrientation == Orientation.HORIZONTAL
		val crossRow = !alongRow

		var tableSize = Vector2d.ZERO

		rows.forEachIndexed { row, rowStack ->
			var position = tableSize.axisProjection(crossRow)

			columns.forEachIndexed { column, columnStack ->
				val node = columnStack.children.getOrNull(row)
				val bound = columnStack.bounds.getOrNull(row)
				val xLastBound = rowStack.bounds.getOrNull(column - 1)
				val yLastBound = columnStack.bounds.getOrNull(row - 1)

				val xGap = getChildrenGap(xLastBound, bound, if (alongRow) LayoutSide.RIGHT else LayoutSide.BOTTOM)
				val yGap = getChildrenGap(yLastBound, bound, if (alongRow) LayoutSide.BOTTOM else LayoutSide.RIGHT)
				val gap = if (alongRow) Vector2d(xGap, yGap) else Vector2d(yGap, xGap)

				val placeSize = clampCellSize(columnStack.maxBound.placeSize.axisProjection(alongRow) + rowStack.maxBound.placeSize.axisProjection(crossRow))
				val clientSize = clampCellSize(columnStack.maxBound.clientSize.axisProjection(alongRow) + rowStack.maxBound.clientSize.axisProjection(crossRow))

				position += gap.axisProjection(alongRow)
				node?.setArea(position + gap.axisProjection(crossRow), clientSize)
				position += clientSize.axisProjection(alongRow)

				tableSize = tableSize.upper(position + placeSize.axisProjection(crossRow))

			}
		}

		return tableSize
	}

	private fun clampCellSize(baseSize: Vector2d): Vector2d {
		if (cellSize == null) return baseSize
		if (cellSize.preferredSize != null) return cellSize.preferredSize
		return baseSize.clamp(cellSize.minSize, cellSize.maxSize)
	}

	private fun createStack(children: List<LayoutTarget>, viewSize: Vector2d): Stack {
		val bounds = getBounds(children, viewSize)
		val maxBound = getMaxBound(bounds)
		return Stack(children, bounds, maxBound)
	}

	private fun createColumn(rowAmount:Int, column: Int, children: List<LayoutTarget>, viewSize: Vector2d): Stack {
		val columnChildren = (0 until rowAmount).mapNotNull { row ->
			val id = row * this.rowLength + column
			children.getOrNull(id)
		}
		return createStack(columnChildren, viewSize)
	}

}