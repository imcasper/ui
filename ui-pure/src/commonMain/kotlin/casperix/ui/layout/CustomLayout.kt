package casperix.ui.layout

import casperix.math.vector.Vector2d
import casperix.ui.core.UINode

class CustomLayout(val map: Map<UINode, Layout?>, val def: Layout = Layout.SCREEN) : Layout {
	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {
		val placementMap = map.mapKeys { it.key.placement }
		var maxSize = Vector2d.ZERO
		//TODO: create list for same layout
		children.forEach {
			val hasLayout = placementMap.contains(it)
			val layout = placementMap[it]

			//	ignore layout if map contains (layout=null)
			if (hasLayout && layout == null) return@forEach

			val contentSize = (layout ?: def).update(listOf(it), viewSize)
			maxSize = maxSize.upper(contentSize)
		}
		return maxSize
	}
}