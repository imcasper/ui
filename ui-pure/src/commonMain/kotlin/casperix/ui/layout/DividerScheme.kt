package casperix.ui.layout

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.misc.clamp
import casperix.ui.type.LayoutSide
import kotlin.math.max

data class DividerScheme(val primary: Box2d, val secondary: Box2d) {
	companion object {
		fun create(side: LayoutSide, primaryMinMaxSize: Box2d, secondaryArea: Vector2d?, space: Vector2d): DividerScheme {
			val orientation = side.orientation()

			val headerBound = if (secondaryArea != null) {
				if (orientation == Orientation.VERTICAL) {
					Box2d.byDimension(Vector2d(max(primaryMinMaxSize.min.x, secondaryArea.x), primaryMinMaxSize.min.y), primaryMinMaxSize.dimension)
				} else {
					Box2d.byDimension(Vector2d(primaryMinMaxSize.min.x, max(primaryMinMaxSize.min.y, secondaryArea.y)), primaryMinMaxSize.dimension)
				}
			} else {
				primaryMinMaxSize
			}

			val primarySize = when (orientation) {
				Orientation.VERTICAL -> Vector2d(space.x.clamp(headerBound.min.x, headerBound.max.x), headerBound.min.y)
				Orientation.HORIZONTAL -> Vector2d(headerBound.min.x, space.y.clamp(headerBound.min.y, headerBound.max.y))
			}
			val secondarySize = space - when (orientation) {
				Orientation.VERTICAL -> primarySize.yAxis
				Orientation.HORIZONTAL -> primarySize.xAxis
			}

			val primaryPosition = when (side) {
				LayoutSide.TOP, LayoutSide.LEFT -> Vector2d.ZERO
				LayoutSide.BOTTOM -> secondarySize.yAxis
				LayoutSide.RIGHT -> secondarySize.xAxis
			}

			val secondaryPosition = when (side) {
				LayoutSide.BOTTOM, LayoutSide.RIGHT -> Vector2d.ZERO
				LayoutSide.TOP -> primarySize.yAxis
				LayoutSide.LEFT -> primarySize.xAxis
			}
			return DividerScheme(Box2d.byDimension(primaryPosition, primarySize.upper(Vector2d.ZERO)), Box2d.byDimension(secondaryPosition, secondarySize.upper(Vector2d.ZERO)))
		}
	}
}