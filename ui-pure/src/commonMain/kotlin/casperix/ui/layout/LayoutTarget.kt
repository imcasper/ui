package casperix.ui.layout

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.type.LayoutAlign

/**
 * 	Ограничения области элемента:
 * 	@param clientSize        размер самой области
 * 	@param gap			допустимые зазоры до границы соседей
 * 	@param border	бордюр - пустая область вокруг элемента
 *
 * 	Зазор -- ограничивает минимальное расстояние между элементами
 * 	Допустим зазоры элементов 4 и 6. Пустое место между элемнтами:
 * 	max(4, 6) = 6
 *
 * 	Бордюр -- пустая область вокруг каждого элемента
 * 	Допустим бордюры элементов 4 и 6. Пустое место между элемнтами:
 * 	4 + 6 = 10
 */
data class Bound(val clientSize: Vector2d, val gap: SideIndents, val border:SideIndents) {
	val placeSize:Vector2d get() = clientSize + border.size + gap.size

	val margin = gap + border

	fun upper(other: Bound): Bound {
		return Bound(this.clientSize.upper(other.clientSize), this.gap.upper(other.gap), this.border.upper(other.border))
	}
	companion object {
		val ZERO = Bound(Vector2d.ZERO, SideIndents.ZERO, SideIndents.ZERO)
	}
}

interface LayoutTarget {
	val align: LayoutAlign
	val sizeMode:SizeMode
	val gap: SideIndents
	val border: SideIndents
	fun getBound(): Bound

	/**
	 * 	Задается выделенная элементу область
	 * 	Элемент может занимать ее целиком или частично
	 * 	Отступы и зазоры находятся вне этой области
	 */
	fun setArea(position: Vector2d, size: Vector2d)
	fun setArea(area: Box2d) {
		return setArea(area.min, area.dimension)
	}
}