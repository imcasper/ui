package casperix.ui.layout

import casperix.misc.collapse
import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.misc.max
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide

interface Layout {

	/**
	 * 	Вернет размер минимальной рекомендованной области для детей (с учетом отступов и зазоров детей)
	 */
	fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d

	fun getBounds(children: List<LayoutTarget>, viewSize: Vector2d): List<Bound> {
		return children.map { it.getBound() }
	}

	fun getMaxBound(children: List<Bound>): Bound {
		return children.collapse(Bound.ZERO) { child, max ->
			child.upper(max)
		}
	}

	fun getChildrenGap(last: Bound?, next: Bound?, direction: LayoutSide): Double {
		val nextGap = next?.gap?.gapFromSide(direction.invert()) ?: 0.0
		val nextBorder = next?.border?.gapFromSide(direction.invert()) ?: 0.0

		val lastGap = last?.gap?.gapFromSide(direction) ?: 0.0
		val lastBorder = last?.border?.gapFromSide(direction) ?: 0.0

		return max(lastGap, nextGap) + (lastBorder + nextBorder)
	}

	fun setAreas(children: List<LayoutTarget>, areas:List<Box2d>) {
		children.forEachIndexed { index, node ->
			node.setArea(areas[index])
		}
	}

	fun alignToCenter(viewSize: Vector2d, actualSize: Vector2d, areas: List<Box2d>): List<Box2d> {
		val toCenterOffset = LayoutAlign.CENTER_CENTER.getPosition(viewSize, actualSize)

		return areas.map { area ->
			Box2d(area.min + toCenterOffset, area.max + toCenterOffset)
		}

	}

	companion object {
		val SCREEN = ScreenLayout()
		val HORIZONTAL = LineLayout(Orientation.HORIZONTAL)
		val VERTICAL = LineLayout(Orientation.VERTICAL)

		val LEFT = LineLayout(LayoutSide.LEFT)
		val RIGHT = LineLayout(LayoutSide.RIGHT)
		val TOP = LineLayout(LayoutSide.TOP)
		val BOTTOM = LineLayout(LayoutSide.BOTTOM)
	}
}

