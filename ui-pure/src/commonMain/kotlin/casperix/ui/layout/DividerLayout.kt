package casperix.ui.layout

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.ui.core.UINode
import casperix.ui.type.LayoutSide

class DividerLayout(val mainSide: LayoutSide) : Layout {

	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {
		if (children.isEmpty()) return Vector2d.ZERO

		val primary = children.get(0)
		val primaryBound = primary.getBound()

		val secondary = children.getOrNull(1)
		val secondaryBound = secondary?.getBound()

		val direction = mainSide.invert()
		val right = direction.rotateCW()
		val uAxis = direction.direction().toVector2d()
		val vAxis = right.direction().toVector2d()


		val uFirstGap = uAxis * getChildrenGap(null, primaryBound, direction)
		val uMiddleGap = uAxis * getChildrenGap(primaryBound, secondaryBound, direction)
		val uLastGap = uAxis * getChildrenGap(secondaryBound, null, direction)

		val vPrimaryBeforeGap = vAxis * getChildrenGap(null, primaryBound, right)
		val vPrimaryAfterGap = vAxis * getChildrenGap(primaryBound, null, right)
		val vSecondaryBeforeGap = vAxis * getChildrenGap(null, secondaryBound, right)
		val vSecondaryAfterGap = vAxis * getChildrenGap(secondaryBound, null, right)

		val uPrimaryClientSize = uAxis * (primaryBound.clientSize).upper(Vector2d.ZERO)
		val vPrimaryClientSize = vAxis * (viewSize - vPrimaryBeforeGap.absoluteValue - vPrimaryAfterGap.absoluteValue).upper(Vector2d.ZERO)

		val uSecondaryClientSize = uAxis * (viewSize - primaryBound.clientSize  - uFirstGap.absoluteValue - uMiddleGap.absoluteValue - uLastGap.absoluteValue).upper(Vector2d.ZERO)
		val vSecondaryClientSize = vAxis * (viewSize - vSecondaryBeforeGap.absoluteValue - vSecondaryAfterGap.absoluteValue).upper(Vector2d.ZERO)

		val uStart = when (direction) {
			LayoutSide.RIGHT -> Vector2d.ZERO
			LayoutSide.BOTTOM -> viewSize.xAxis
			LayoutSide.LEFT -> viewSize
			LayoutSide.TOP -> viewSize.yAxis
		}

		val primaryA = uStart + uFirstGap + vPrimaryBeforeGap
		val primaryB = uStart + uFirstGap + vPrimaryBeforeGap + uPrimaryClientSize + vPrimaryClientSize
		primary.setArea(Box2d.byCorners(primaryA, primaryB))

		if (secondary != null) {
			val secondaryA = uStart + uFirstGap + uPrimaryClientSize + uMiddleGap + vSecondaryBeforeGap
			val secondaryB = uStart + uFirstGap + uPrimaryClientSize + uMiddleGap + vSecondaryBeforeGap + uSecondaryClientSize + vSecondaryClientSize
			secondary.setArea(Box2d.byCorners(secondaryA, secondaryB))
		}

		return if (secondaryBound == null) {
			val vSize = vAxis * primaryBound.placeSize
			(uFirstGap + uPrimaryClientSize + uMiddleGap + vSize).absoluteValue
		} else {
			val uSecondaryClientSizePreferred = uAxis * (secondaryBound.clientSize).upper(Vector2d.ZERO)
			val vSize = vAxis * primaryBound.placeSize.upper(secondaryBound.placeSize)
			(uFirstGap + uPrimaryClientSize + uMiddleGap + uSecondaryClientSizePreferred + uLastGap + vSize).absoluteValue

		}
	}

	constructor(root: UINode, primary: UINode, secondary: UINode, side: LayoutSide = LayoutSide.TOP) : this(side) {
		root.layout = this
		root += primary
		root += secondary
	}


}