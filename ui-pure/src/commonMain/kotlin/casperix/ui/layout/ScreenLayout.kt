package casperix.ui.layout

import casperix.math.vector.Vector2d

/**
 * 	Предоставляет каждому элементу всю область родительского элемента
 */
class ScreenLayout : Layout {
	override fun update(children: List<LayoutTarget>, viewSize: Vector2d): Vector2d {

		val bounds = getBounds(children, viewSize)
		val maxBound = getMaxBound(bounds)

		children.forEachIndexed {index, node ->
			val bound = bounds[index]
			val leftTop = bound.gap.leftTop + bound.border.leftTop
			val rightBottom = viewSize - bound.border.rightBottom - bound.gap.rightBottom

			val nodeSize = (rightBottom - leftTop).upper(Vector2d.ZERO)
			node.setArea(leftTop, nodeSize)
		}

		return maxBound.placeSize
	}
}