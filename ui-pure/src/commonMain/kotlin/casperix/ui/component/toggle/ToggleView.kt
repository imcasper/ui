package casperix.ui.component.toggle

import casperix.signals.concrete.StoragePromise
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.math.vector.Vector2d
import casperix.ui.component.ViewComponent
import casperix.ui.component.button.ButtonView
import casperix.ui.component.button.ButtonLogic
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.engine.Bitmap
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.layout.Layout

@Deprecated(message = "Use ToggleView")
typealias Toggle = ToggleView

class ToggleView(val logic: ToggleLogic, node: UINode) : ViewComponent<ToggleSkin>(ToggleSkin::class, node) {
	constructor(logic: ToggleLogic) : this(logic, logic.node)

	constructor(label:String?, icon:BitmapGraphic?, buttonSize:Vector2d, signal:StoragePromise<Boolean> = StorageSignal(false)) : this(ToggleLogic(ButtonLogic(), signal)) {
		node.placement.sizeMode = SizeMode.const(buttonSize)
		if (label != null) {
			button.addLabel(label)
		}
		if (icon != null) {
			button.addBitmapGraphic(icon)
		}
	}
	constructor(icon:BitmapGraphic?, label:String?, buttonSize:Vector2d, signal:StoragePromise<Boolean> = StorageSignal(false)) : this(ToggleLogic(ButtonLogic(), signal)) {
		node.placement.sizeMode = SizeMode.const(buttonSize)
		if (icon != null) {
			button.addBitmapGraphic(icon)
		}
		if (label != null) {
			button.addLabel(label)
		}
	}

	constructor(label:String, buttonSize:Vector2d, signal:StoragePromise<Boolean> = StorageSignal(false)) : this(label, null, buttonSize, signal) {
	}

	constructor(label:String, buttonSize:Vector2d, on:Boolean = false, action:((Boolean)->Unit)? = null) : this(label, buttonSize, StorageSignal(on)) {
		action?.let { switch.then { action(it) }}
	}

	constructor(label:String, icon:BitmapGraphic, buttonSize:Vector2d, on:Boolean = false, action:((Boolean)->Unit)? = null) : this(label, icon, buttonSize, StorageSignal(on)) {
		action?.let { switch.then { action(it) }}
	}

	constructor(icon:BitmapGraphic, label:String, buttonSize:Vector2d, on:Boolean = false, action:((Boolean)->Unit)? = null) : this(icon, label, buttonSize, StorageSignal(on)) {
		action?.let { switch.then { action(it) }}
	}

	constructor(icon:BitmapGraphic, buttonSize:Vector2d, on:Boolean = false, action:((Boolean)->Unit)? = null) : this(null, icon, buttonSize, StorageSignal(on)) {
		action?.let { switch.then { action(it) }}
	}

	val switch = logic.switch

	val button = ButtonView(logic.button, node)

	init {
		switch.then(components) { changeSkin() }
	}

	fun clearContent() {
		button.clearContent()
	}

	fun setContentLayout(layout: Layout) {
		button.setContentLayout(layout)
	}

	fun addLabel(value: String) {
		button.addLabel(value)
	}

	fun addBitmap(bitmap: Bitmap, scale:Double = 1.0) {
		button.addBitmap(bitmap, scale)
	}

	fun addBitmapGraphic(bitmapGraphic: BitmapGraphic) {
		button.addBitmapGraphic(bitmapGraphic)
	}

	private fun changeSkin() {
		val skin = skin ?: return
		applySkin(skin)
	}

	override fun applySkin(skin: ToggleSkin) {
		val on = switch.value
		button.skin = if (on) skin.on else skin.off
	}
}
