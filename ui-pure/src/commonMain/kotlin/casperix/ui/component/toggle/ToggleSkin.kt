package casperix.ui.component.toggle

import casperix.ui.component.button.ButtonSkin

data class ToggleSkin(val on: ButtonSkin, val off: ButtonSkin)