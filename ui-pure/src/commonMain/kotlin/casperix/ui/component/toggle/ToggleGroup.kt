package casperix.ui.component.toggle

import casperix.signals.collection.toObservableList
import casperix.signals.concrete.BooleanSwitcher
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.util.BooleanGroupSelector

object ToggleGroup {
	fun create(behaviour: BooleanGroupSelector.Behaviour = BooleanGroupSelector.Behaviour.ALWAYS_ONE, items: List<ToggleView>) {
		val holder = items.firstOrNull() ?: return
		create(holder.node, behaviour, items)
	}

	fun create(behaviour: BooleanGroupSelector.Behaviour = BooleanGroupSelector.Behaviour.ALWAYS_ONE, vararg items: ToggleView) {
		val holder = items.firstOrNull() ?: return
		create(holder.node, behaviour, items.toList())
	}

	fun create(holder: UINode, behaviour: BooleanGroupSelector.Behaviour, items: List<ToggleView>): UIComponent {
		return UIBooleanGroup.create(holder, behaviour, items.map { it.switch })
	}

	fun createAndPlace(parent: UINode, behaviour: BooleanGroupSelector.Behaviour, items: List<ToggleView>): UIComponent {
		parent.children.addAll(items.map { it.node })
		return create(parent, behaviour, items)
	}
}

object ToggleLogicGroup {
	fun create(holder: UINode, behaviour: BooleanGroupSelector.Behaviour, items: List<ToggleLogic>): UIComponent {
		return UIBooleanGroup.create(holder, behaviour, items.map { it.switch })
	}
}

object UIBooleanGroup {
	fun create(holder: UINode, behaviour: BooleanGroupSelector.Behaviour, items: List<BooleanSwitcher>): UIComponent {
		return UIComponent.wrap(holder, BooleanGroupSelector(items.toObservableList(), behaviour))
	}
}