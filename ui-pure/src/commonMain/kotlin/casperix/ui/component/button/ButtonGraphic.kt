package casperix.ui.component.button

import casperix.math.color.Color
import casperix.math.vector.Vector2d
import casperix.misc.clamp
import casperix.signals.then
import casperix.ui.core.UIComponent
import casperix.ui.engine.Drawer
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.Graphic


class ButtonGraphic(val logic: ButtonLogic, var skin: ButtonSkin?) : UIComponent(logic.node), Graphic {
	private val increaseBackDuration = 0.20
	private val decreaseBackDuration = 0.04
	private val increaseFrontDuration = 0.40
	private val decreaseFrontDuration = 0.20

	private var focusBackFactor = 0.0
	private var focusEffectFactor = 0.0
	private var localMousePosition: Vector2d = Vector2d.ZERO

	init {

		node.inputs.onMouseMove.then(components) {
			localMousePosition = it.position - node.absolutePosition
		}
		node.events.nextFrame.then(components) {
			update(it)
		}
	}

	private fun update(tick: Double) {
		focusBackFactor = (focusBackFactor + tick / (if (logic.focused) +increaseBackDuration else -decreaseBackDuration)).clamp(0.0, 1.0)
		focusEffectFactor = (focusEffectFactor + tick / (if (logic.focused) +increaseFrontDuration else -decreaseFrontDuration)).clamp(0.0, 1.0)
	}


	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val skin = skin ?: return
		val drawType = skin.drawType

		if (drawType == ButtonDrawType.SIMPLIFY) {
			if (!logic.enabled) {
				drawAlpha(drawer, skin.allowClipping, skin.disabled.back, 1.0, position, size)
			} else if (!logic.pressed) {
				drawAlpha(drawer, skin.allowClipping, skin.normal.back, 1.0, position, size)
			} else {
				drawAlpha(drawer, skin.allowClipping, skin.pressed.back, 1.0, position, size)
			}
		} else if (drawType == ButtonDrawType.SMOOTHED_LAYERS) {
			val fixedBack = if (!logic.enabled) {
				skin.disabled.back
			} else {
				skin.normal.back
			}
			drawAlpha(drawer, skin.allowClipping, fixedBack, 1.0, position, size)
			if (!logic.pressed) {
				drawAlpha(drawer, skin.allowClipping, skin.focused.back, focusBackFactor, position, size)
			} else {
				drawAlpha(drawer, skin.allowClipping, skin.pressed.back, 1.0, position, size)
			}

			val specular = skin.specular ?: return
			drawAlpha(drawer, skin.allowClipping, specular.graphic, focusEffectFactor, position + localMousePosition - specular.size / 2.0, specular.size)
		}
	}

	private fun drawAlpha(drawer: Drawer, allowClipping: Boolean, graphic: BitmapGraphic?, alpha: Double, position: Vector2d, size: Vector2d) {
		if (graphic == null || alpha <= 0.0) {
			if (allowClipping) node.clippingContent = false
		} else {
			if (allowClipping) node.clippingContent = true

			if (alpha >= 1.0) {
				graphic.draw(drawer, position, size)
			} else {
				val shadowed = graphic.copy(color = Color.WHITE.setAlpha(alpha).toColor4d())
				shadowed.draw(drawer, position, size)
			}
		}
	}


}