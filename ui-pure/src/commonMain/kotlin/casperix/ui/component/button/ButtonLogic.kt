package casperix.ui.component.button

import casperix.signals.concrete.EmptySignal
import casperix.signals.then
import casperix.ui.core.CursorTypes
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.input.CommonNodeEvent
import casperix.ui.input.GlobalAction

class ButtonLogic(node: UINode, click: (() -> Unit)? = null) : UIComponent(node) {
	companion object {
		var clickOnTouchDown = false
	}

	val onClicked = EmptySignal()
	val onChanged = EmptySignal()

	var enabled: Boolean by onChanged.observable(true)
	var pressed: Boolean by onChanged.observable(false)
	var focused: Boolean by onChanged.observable(false)

	constructor(click: (() -> Unit)? = null) : this(UINode(), click)

	init {
		if (click != null) {
			onClicked.then { click() }
		}

		node.inputs.onTouchDown.then(components) { event ->
			if (event.captured) return@then

			if (enabled) {
				if (clickOnTouchDown) {
					makeClick()
				}
				pressed = true
				event.captured = true
			}
		}

		node.inputs.onTouchUp.then(components) { event ->
			if (enabled) {
				if (pressed) {
					if (!clickOnTouchDown) {
						makeClick()
					}
					pressed = false
				}
				event.captured = true
			}
		}

		node.inputs.onMouseFocused.then(components) { event ->
			if (event != null) {
				event.captured = true

				if (enabled) {
					focused = true
					CommonNodeEvent.onCursor.set(CursorTypes.HAND_CURSOR)
				}
			} else {
				if (enabled) {
					CommonNodeEvent.onCursor.set(CursorTypes.DEFAULT_CURSOR)
				}
				focused = false
				pressed = false
			}
		}
	}

	private fun makeClick() {
		node.root.events.onGlobalAction.set(GlobalAction("click", node))
		onClicked.set()
	}

}