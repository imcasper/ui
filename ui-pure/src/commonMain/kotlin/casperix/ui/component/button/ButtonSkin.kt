package casperix.ui.component.button

import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.ui.component.text.TextSkin
import casperix.ui.core.SideIndents
import casperix.ui.graphic.BitmapGraphic

enum class ButtonDrawType {
	SMOOTHED_LAYERS,
	SIMPLIFY,
}

data class ButtonSkin(
	val allowClipping:Boolean,
	val gap:SideIndents,
	val border:SideIndents,
	val disabled: ButtonStateSkin,
	val normal: ButtonStateSkin,
	val focused: ButtonStateSkin,
	val pressed: ButtonStateSkin,
	val specular: ButtonSpecularSkin?,
	val drawType: ButtonDrawType,
	val hitBorder:SideIndents,
	val contentOffsetOnPress: Vector2i?,
)

data class ButtonSpecularSkin(
	val graphic: BitmapGraphic,
	val size: Vector2d,
)

data class ButtonStateSkin(
	val back: BitmapGraphic?,
	val text: TextSkin,
)