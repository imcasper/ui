package casperix.ui.component.text

import casperix.math.vector.Vector2d
import casperix.signals.concrete.EmptySignal
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.font.SymbolWidthMode
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Align

@Deprecated(message = "Use TextView")
typealias Text = TextView

class TextView(text: String, autoSize: Boolean, wordWrap: Boolean, node: UINode) : ViewComponent<TextSkin>(TextSkin::class, node) {
	val onChanged = EmptySignal()

	var text: String by onChanged.observable(text)
	var autoSize: Boolean by onChanged.observable(autoSize)
	var wordWrap: Boolean by onChanged.observable(wordWrap)
	var symbolWidthMode: SymbolWidthMode by onChanged.observable(SymbolWidthMode.DEFAULT)


	constructor(text: String, autoSize: Boolean = true, wrap: Boolean = false) : this(text, autoSize, wrap, UINode()) {
	}

	constructor(text: String, constSize: Vector2d, wrap: Boolean = true) : this(text, false, wrap, UINode()) {
		node.placement.sizeMode = SizeMode.const(constSize)
	}

	private val main = UINode()
	private val shadow = UINode()

	init {
		if (node.name == null) node.name = "text"
//		shadow.clippingContent = true
//		main.clippingContent = true
		node += shadow
		node += main

		onChanged.then(components) {
			updateAll()
		}
		node.events.propertyChanged.then(components) {
			loadFont()
		}
	}

	private fun loadFont() {
		val engine = engine ?: return
		val skin = skin ?: return
		val fontSource = skin.font.source

		engine.supplier.fontFromSource( fontSource, text)
	}

	private fun updateAll() {
		val skin = skin ?: return
		loadFont()

		val graphic = TextGraphic(text, skin.font, skin.color, wordWrap, Align.MIN, Align.MIN, skin.bold, skin.italic, skin.underline, symbolWidthMode)
		main.graphic = graphic

		if (skin.shadow != null) {
			val offset = skin.shadow.offset
			shadow.placement.gap = SideIndents(offset.x, offset.y, -offset.x, -offset.y)
			shadow.graphic = TextGraphic(text, skin.font, skin.shadow.color, wordWrap, Align.MIN, Align.MIN, skin.bold, skin.italic, skin.underline, symbolWidthMode)
		} else {
			shadow.graphic = null
		}

		node.placement.gap = skin.gap
		if (autoSize) {
			val bounds = graphic.font.calculateBounds(text, Vector2d(Double.MAX_VALUE), wordWrap, symbolWidthMode)
			node.placement.sizeMode = SizeMode.const(bounds.ceilToVector2i().toVector2d())
		}
	}

	override fun applySkin(skin: TextSkin) {
		updateAll()
	}

}

