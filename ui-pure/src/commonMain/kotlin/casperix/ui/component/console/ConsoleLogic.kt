package casperix.ui.component.console

import casperix.misc.sliceSafe
import casperix.signals.concrete.Signal


class ConsoleLogic {
	val commands = mutableListOf<ConsoleCommand>()
	val onCommandExecuted = Signal<CommandResult>()

	init {
		//	embed command
		commands += ConsoleCommand("Help", "show description of all commands") {
			commands.sortedBy { it.name }.joinToString("\n") {
				ConsoleHelper.itemMarker + it.name + " - " + it.description
			}
		}
	}

	private val byWordSplitter = Regex("[\\p{Lu}][\\p{Ll}]*")

	class CommandInfo(val command: ConsoleCommand, val arguments: List<String>)

	class CommandResult(val command: ConsoleCommand, val success: Boolean, val output: String)

	fun parseInput(input: String): CommandInfo? {
		val arguments = input.split(Regex("\\s"))
		if (arguments.isEmpty()) return null
		val commandName = arguments[0]
		val command = commands.firstOrNull { smartEqual(it.name, commandName) } ?: return null
		return CommandInfo(command, arguments.sliceSafe(1..arguments.size))
	}

	private fun smartEqual(originalName: String, inputName: String): Boolean {
		if (originalName.isEmpty() || inputName.isEmpty()) return false

		val originalParts = byWordSplitter.findAll(originalName).map { it.value }.toList()
		val inputParts = byWordSplitter.findAll(inputName).map { it.value }.toList()
		if (originalParts.size != inputParts.size) return false

		for (index in originalParts.indices) {
			val inputPart = inputParts[index]
			val originalPart = originalParts[index]
			if (!originalPart.startsWith(inputPart)) return false
		}

		return true
	}

	fun execute(input: CommandInfo): CommandResult {
		try {
			val resultValue = input.command.action(input.arguments)
			val result = CommandResult(input.command, true, resultValue)
			onCommandExecuted.set(result)
			return result
		} catch (error: Exception) {
			val result = CommandResult(input.command, false, "Execute failed")
			onCommandExecuted.set(result)
			return result
		}
	}
}