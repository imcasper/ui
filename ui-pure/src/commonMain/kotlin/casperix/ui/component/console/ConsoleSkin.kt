package casperix.ui.component.console

import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.ui.component.document.DocumentSkin
import casperix.ui.component.scroll.ScrollerSkin
import casperix.ui.component.text.TextSkin
import casperix.ui.component.texteditor.TextEditorSkin
import casperix.ui.component.texteditor.TextInputSkin
import casperix.ui.core.SideIndents
import casperix.ui.font.Font
import casperix.ui.graphic.Graphic

class ConsoleSkin(
	val inputSkin: TextEditorSkin,
	val outputScrollSkin: ScrollerSkin,
	val outputTextIndents: SideIndents,
	val outputFont: Font,
	val successColor: Color,
	val failedColor: Color,
	val infoColor: Color,
)