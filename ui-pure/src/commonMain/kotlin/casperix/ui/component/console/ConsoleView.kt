package casperix.ui.component.console

import casperix.input.KeyButton
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.component.timer.TimerLogic
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.text.TextView
import casperix.ui.component.text.TextSkin
import casperix.ui.component.texteditor.TextEditor
import casperix.ui.core.*
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide

class ConsoleView(val logic: ConsoleLogic, val maxOutputLines: Int = 1000, node: UINode = UINode()) : ViewComponent<ConsoleSkin>(ConsoleSkin::class, node) {
	class Line(val message: String, val flag: Flag)

	enum class Flag {
		SUCCESS,
		FAILED,
		INFO,
	}

	private val history = mutableListOf<String>()
	private var historyIndex = 0
	private var lines = mutableListOf<Line>()
	private val outputScroll = ScrollBoxView()
	private val lineContent = UINode()
	private val input = TextEditor(null, "", "<input command here...>")

	init {
		node.placement.sizeMode = SizeMode.max
		node.layout = DividerLayout(LayoutSide.BOTTOM)
		node += input
		node += outputScroll
		outputScroll.content += lineContent
		lineContent.layout = Layout.VERTICAL

		input.inputComponent.node.inputs.onKeyDown.then(components) {
			if (it.button == KeyButton.UP) {
				setFromHistory(-1)
			} else if (it.button == KeyButton.DOWN) {
				setFromHistory(+1)
			}

		}
		logic.onCommandExecuted.then {
			addLines(it.output, if (it.success) Flag.SUCCESS else Flag.FAILED)
		}
		input.inputComponent.logic.onEnter.then(components) { rawCommand ->
			enterCommand(rawCommand)
		}
	}

	private fun enterCommand(rawCommand: String) {
		//	Ignore action for empty text
		if (rawCommand == "") return

		input.inputComponent.logic.onText.value = ""

		history.remove(rawCommand)
		history.add(rawCommand)
		historyIndex = history.size - 1

		val parsedCommand = logic.parseInput(rawCommand)
		if (parsedCommand == null) {
			addLines("Can't parse command:\n$rawCommand", Flag.FAILED)
		} else {
			addLines(parsedCommand.command.name, Flag.INFO)
			logic.execute(parsedCommand)
		}
	}

	private fun setFromHistory(delta: Int) {
		if (history.isEmpty()) return

		if (historyIndex < 0) historyIndex = history.size - 1
		if (historyIndex >= history.size) historyIndex = 0
		input.inputComponent.logic.onText.set(history[historyIndex])

		historyIndex += delta
	}

	fun makeInputFocus() {
		input.inputComponent.logic.makeFocused()
	}

	private fun addLines(value: String, flag: Flag) {
		value.split("\n").forEach {
			addLine(it, flag)
		}
		updateText()
	}

	private fun addLine(value: String, flag: Flag) {
		lines.add(Line(value, flag))
		if (lines.size <= maxOutputLines) return

		lines = lines.subList(lines.size - maxOutputLines, lines.size)
		lines.add(0, Line("...The history of the lines is hidden in the darkness of centuries...", Flag.INFO))
	}

	private fun updateText() {
		lineContent.children.clear()
		lineContent.children.addAll(calculateTextLines())

		TimerLogic.afterFrame(node, 5u) {
			outputScroll.logic.navigateToBottom()
		}
	}

	private fun createTextLine(line: Line, consoleSkin: ConsoleSkin?): UINode {
		val textSkin = if (consoleSkin == null) {
			null
		} else {
			val baseSkin = TextSkin(consoleSkin.outputFont, SideIndents.ZERO)
			when (line.flag) {
				Flag.SUCCESS -> baseSkin.copy(color = consoleSkin.successColor)
				Flag.FAILED -> baseSkin.copy(color = consoleSkin.failedColor)
				Flag.INFO -> baseSkin.copy(color = consoleSkin.infoColor)
			}
		}

		val text = TextView(line.message)
		text.skin = textSkin
		text.node.placement.align = LayoutAlign.LEFT
		return text.node
	}

	private fun calculateTextLines(): List<UINode> {
		val consoleSkin = skin
		return lines.map {
			createTextLine(it, consoleSkin)
		}
	}

	override fun applySkin(skin: ConsoleSkin) {
		outputScroll.skin = skin.outputScrollSkin
		input.skin = skin.inputSkin
		lineContent.placement.border = skin.outputTextIndents
		updateText()
	}
}