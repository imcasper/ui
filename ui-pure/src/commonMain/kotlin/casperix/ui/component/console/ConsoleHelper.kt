package casperix.ui.component.console

import casperix.signals.concrete.StorageSignal
import casperix.signals.switch
import casperix.misc.Either
import casperix.misc.Left
import casperix.misc.Right

object ConsoleHelper {
	val itemMarker = " - "

	fun switchVisibleAndGet(signal: StorageSignal<Boolean>): String {
		signal.switch()
		return if (signal.value) "now show" else "now hide"
	}

	fun switchActiveAndGet(signal: StorageSignal<Boolean>): String {
		signal.switch()
		return if (signal.value) "now active" else "now inactive"
	}

	fun getDouble(source: List<String>, index: Int): Either<String, Double> {
		val raw = source.getOrNull(index) ?: return Left("Param ${index + 1} not set")
		val value = raw.toDoubleOrNull() ?: return Left("Can't parse double from $raw")
		return Right(value)
	}

	fun getLong(source: List<String>, index: Int): Either<String, Long> {
		val raw = source.getOrNull(index) ?: return Left("Param ${index + 1} not set")
		val value = raw.toLongOrNull() ?: return Left("Can't parse long from $raw")
		return Right(value)
	}

	fun setDouble(source: List<String>, setup: (Double) -> String): String {
		if (source.isEmpty()) return "you must set value"
		val value = source.first().toDoubleOrNull() ?: return "invalid value"
		return setup(value)
	}

	fun setLong(source: List<String>, setup: (Long) -> String): String {
		if (source.isEmpty()) return "you must set value"
		val value = source.first().toLongOrNull() ?: return "invalid value"
		return setup(value)
	}

	fun commandSetDouble(name: String, description: String, onValue: (Double) -> Unit):ConsoleCommand {
		return ConsoleCommand(name, description) { output -> setDouble(output) { value -> onValue(value); "$name=$value" } }
	}

	fun commandSetLong(name: String, description: String, onValue: (Long) -> Unit):ConsoleCommand {
		return ConsoleCommand(name, description) { output -> setLong(output) { value -> onValue(value); "$name=$value" } }
	}
}