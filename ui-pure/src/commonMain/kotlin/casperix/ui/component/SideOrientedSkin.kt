package casperix.ui.component

import casperix.ui.type.LayoutSide

class SideOrientedSkin<Skin>(val left: Skin, val top: Skin, val right: Skin, val bottom: Skin) {
	fun getBySide(side: LayoutSide): Skin {
		return when (side) {
			LayoutSide.LEFT -> left
			LayoutSide.TOP -> top
			LayoutSide.RIGHT -> right
			LayoutSide.BOTTOM -> bottom
		}
	}
}

