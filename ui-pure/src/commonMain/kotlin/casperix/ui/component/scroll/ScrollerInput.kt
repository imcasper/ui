package casperix.ui.component.scroll

import casperix.misc.disposeAll
import casperix.misc.mutableDisposableListOf
import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.ui.core.UIComponent

class ScrollerInput(val logic: ScrollerLogic) : UIComponent(logic.node) {
	private var startOffset = Vector2d.ZERO
	private val movingComponents = mutableDisposableListOf()

	init {
		node.inputs.onMouseWheel.then(components) {
			val bound = logic.getCurrentBound()
			val factor = it.wheel.y

			if (bound.hasXScroll && bound.hasYScroll) {
				return@then
			}

			val alreadyDragging = logic.isDragging

			if (!alreadyDragging) {
				logic.startDrag()
			}
			if (bound.hasXScroll) {
				logic.setContentPosition(logic.contentPosition - Vector2d(factor * 100.0, 0.0))
			} else {
				logic.setContentPosition(logic.contentPosition - Vector2d(0.0, factor * 100.0))
			}
			if (!alreadyDragging) {
				logic.stopDrag(true)
			}
		}
		node.inputs.onTouchDown.then(components) {
			firstCursorPosition(it.position)

			node.root.inputs.onTouchDragged.then(movingComponents) {
				nextCursorPosition(it.position)
			}

			node.root.inputs.onTouchUp.then(movingComponents) {
				movingComponents.disposeAll()
				nextCursorPosition(it.position)
				logic.stopDrag(false)
			}
		}
	}

	private fun firstCursorPosition(cursorPosition: Vector2d) {
		startOffset = logic.contentPosition - cursorPosition
		logic.startDrag()
	}

	private fun nextCursorPosition(cursorPosition: Vector2d) {
		logic.setContentPosition(startOffset + cursorPosition)
	}

}