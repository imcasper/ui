package casperix.ui.component.scroll

import casperix.math.vector.Vector2d
import casperix.math.axis_aligned.Box2d
import casperix.misc.clamp
import casperix.signals.then
import casperix.math.color.Color4d
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.input.TouchCatcherFactory
import casperix.ui.layout.DividerScheme
import casperix.ui.layout.Orientation
import casperix.ui.type.LayoutSide
import kotlin.math.min

class ScrollerShadow(val logic: ScrollerLogic, val content: UINode, val side: LayoutSide) : UIComponent(UINode()) {
	private val shadow = UINode()
	val skin: ScrollerSkin? get() = node.properties.get(ScrollerSkin::class)

	init {
		node.touchFilterBuilder = TouchCatcherFactory::never
		node.layout = null
		node += shadow

		content.events.onPosition.then(components) { refreshShadow() }
		content.events.onSize.then(components) { refreshShadow() }
		node.events.onSize.then(components) { refreshShadow() }
		node.events.propertyChanged.then(components) { refreshShadow() }
	}

	private fun refreshShadow() {
		val skin = skin ?: return
		val shadowSize = skin.shadowSize
		val shadowGraphic = skin.shadowGraphic

		if (shadowGraphic == null || !shadowSize.greater(Vector2d.ZERO)) {
			shadow.graphic = null
			return
		}

		val value = min(shadowSize.x, logic.idents.gapFromSide(side))
		val max = shadowSize.valueFromOrientation(side.orientation())
		val color = Color4d(1.0, 1.0, 1.0, (value / max).clamp(0.0, 1.0))
		val graphic = when (side) {
			LayoutSide.LEFT -> shadowGraphic.copy(color = color)
			LayoutSide.TOP -> shadowGraphic.copy(color = color, turn90CW = 1)
			LayoutSide.RIGHT -> shadowGraphic.copy(color = color, flipX = true)
			LayoutSide.BOTTOM -> shadowGraphic.copy(color = color, turn90CW = -1)
		}

		val scheme = DividerScheme.create(side, Box2d.byDimension(shadowSize, Vector2d(1000_000.0)) , null, node.placement.size)
		shadow.placement.setArea(scheme.primary.min, scheme.primary.dimension)
		shadow.graphic = graphic
	}

	private fun Vector2d.valueFromOrientation(orientation: Orientation): Double {
		return when (orientation) {
			Orientation.HORIZONTAL -> x
			Orientation.VERTICAL -> y
		}
	}
}