package casperix.ui.component.scroll

import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.math.interpolation.cosineInterpolate
import casperix.math.interpolation.interpolateFunction
import casperix.ui.component.animator.Animator
import casperix.ui.core.SideIndents
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Bound
import kotlin.math.max

class ScrollerLogic(node: UINode, val content: UINode, val centered:Boolean = false) : UIComponent(node) {
	val skin: ScrollerSkin? get() = node.properties.get(ScrollerSkin::class)
	private val normalizeAnimator = Animator(node, ::setContentPosition, Vector2d.interpolateFunction(cosineInterpolate))

	var idents = SideIndents(0); private set
	var contentPosition = Vector2d.ZERO; private set
	var targetPosition = Vector2d.ZERO; private set
	var isDragging = false; private set

	init {
		node.events.onSize.then(components) { refreshSize() }
		content.events.onChildrenSize.then { refreshSize() }
		node.events.propertyChanged.then { refreshSkin() }
	}

	private fun refreshSkin() {
		val skin = skin ?: return
		normalizeAnimator.interpolator = skin.animationSettings.interpolator
	}

	private fun refreshSize() {
		setContentPosition(contentPosition)
		setContentPosition(targetPosition)
	}

	fun startDrag() {
		isDragging = true
		stopNormalize()
	}

	fun stopDrag(fast:Boolean) {
		isDragging = false
		startNormalize(fast)
	}

	fun getCurrentBound():ScrollBound {
		return ScrollBound(node.placement.size, content.placement.getBound(), centered)
	}

	class ScrollBound(val viewportSize: Vector2d, val contentBound:Bound, val centered: Boolean) {
		val contentSize = contentBound.clientSize + contentBound.gap.size

		val delta = viewportSize - contentSize
		val minPosition = Vector2d(if (delta.x >= 0.0) (if (centered) delta.x / 2.0 else 0.0) else delta.x, if (delta.y >= 0.0) (if (centered) delta.y / 2.0 else 0.0) else delta.y)
		val maxPosition = Vector2d.ZERO.upper(minPosition)

		val hasXScroll = minPosition.x < maxPosition.x
		val hasYScroll = minPosition.y < maxPosition.y
	}


	fun navigateToLeft() {
		val bound = getCurrentBound()

		setContentPosition(contentPosition.copy(x = bound.maxPosition.x))
	}

	fun navigateToRight() {
		val bound = getCurrentBound()

		setContentPosition(contentPosition.copy(x = bound.minPosition.x))
	}

	fun navigateToTop() {
		val bound = getCurrentBound()

		setContentPosition(contentPosition.copy(y = bound.maxPosition.y))
	}

	fun navigateToBottom() {
		val bound = getCurrentBound()

		setContentPosition(contentPosition.copy(y = bound.minPosition.y))
	}

	fun setContentPosition(position: Vector2d) {
		val bound = getCurrentBound()
		targetPosition = position.clamp(bound.minPosition, bound.maxPosition)

		val contentX = if (bound.hasXScroll) position.x else bound.maxPosition.x
		val contentY = if (bound.hasYScroll) position.y else bound.maxPosition.y
		contentPosition = Vector2d(contentX, contentY)

		val left = max(0.0, bound.maxPosition.x - contentPosition.x)
		val right = max(0.0, contentPosition.x - bound.minPosition.x)
		val top = max(0.0, bound.maxPosition.y - contentPosition.y)
		val bottom = max(0.0, contentPosition.y - bound.minPosition.y)
		idents = SideIndents(left, top, right, bottom)

		content.placement.setArea(contentPosition, bound.contentSize)
	}


	private fun stopNormalize() {
		normalizeAnimator.stop()
	}

	private fun startNormalize(fast:Boolean) {
		val dest = targetPosition - contentPosition
		if (dest.length() < 0.001) return
		val skin = skin ?: return

		if (fast) {
			contentPosition = targetPosition - dest.sign * dest.absoluteValue * 0.2
		}

		val duration = skin.animationSettings.durationByDest(dest.length())
		normalizeAnimator.start(contentPosition, targetPosition, duration)
	}

}