package casperix.ui.component.tree

import casperix.math.vector.Vector2d
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.Graphic

class TreeSkin(val emptyMarker:BitmapGraphic?, val toggle:ToggleSkin, val tabWidth:Double, val toggleSize: Vector2d)