package casperix.ui.component.tree

import casperix.math.vector.Vector2d

internal class TreeNodeInfo(val all:Collection<TreeNode>, val visibleList: Collection<TreeNode>, val maxDepth: Int, val maxItemSize: Vector2d)