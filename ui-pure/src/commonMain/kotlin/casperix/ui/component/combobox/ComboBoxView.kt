package casperix.ui.component.combobox

import casperix.math.axis_aligned.Box2d
import casperix.math.vector.Vector2d
import casperix.misc.disposeAll
import casperix.misc.mutableDisposableListOf
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.component.button.ButtonView
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.bitmap.BitmapView
import casperix.ui.component.panel.PanelView
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.SideIndents
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.ScalingNone
import casperix.ui.layout.Layout
import casperix.ui.type.LayoutAlign

class ComboBoxView<Value>(val preferredSize: Vector2d, current: Value, val variants: List<Value>, val getName: (Value) -> String, onClick: ((Value) -> Unit)? = null, node: UINode = UINode()) : ViewComponent<ComboBoxSkin>(ComboBoxSkin::class, node) {
	private var clickUpSlots = mutableDisposableListOf()

	private val toggle = ToggleView("", preferredSize, false, ::onClick)// UISelectable("", preferredSize, true, ::onClick)
	private val screen = UINode(layout = null)
	private val menu = UINode(layout = Layout.VERTICAL)
	val onSelected = StorageSignal<Value>(current)

	var isOpened: Boolean = false; private set

	init {
		if (onClick != null) {
			onSelected.then {
				onClick(it)
			}
		}
		screen += menu

		node += toggle
		node.placement.sizeMode = SizeMode.const(preferredSize)
		node.layout = Layout.VERTICAL

		updateMenu()
		updateSelector()

		node.events.onRoot.then { root ->
			if (root.name != "root") {
				close()
			}
		}
	}

	private fun updateMenu() {
		menu.children.clear()

		val skin = skin ?: return

		menu.graphic = skin.shadow
		menu.properties.set(ButtonSkin::class, skin.itemSkin)

		variants.forEach { value ->
			menu += ButtonView(getName(value), preferredSize) {
				onSelect(value)
			}
		}
		menu.events.onChildrenSize.then {
			updateMenuPosition()
		}
		updateMenuPosition()
	}

	private fun updateMenuPosition() {
		menu.placement.setArea(toggle.node.absolutePosition + toggle.node.placement.size.yAxis, menu.placement.size)
	}

	private fun onSelect(value: Value) {
		onSelected.set(value)
		updateSelector()
		close()
	}

	private fun updateSelector() {
		val skin = skin ?: return
		val content = toggle.button.content

		toggle.node.properties.set(ToggleSkin::class, skin.selectorSkin)

		content.children.clear()
		content.placement.sizeMode = SizeMode.max
		content.layout = Layout.SCREEN
		content += TextView(getName(onSelected.value))
		content += BitmapView(BitmapGraphic(skin.buttonIcon.bitmap, ScalingNone())).node.apply {
			placement.align = LayoutAlign.RIGHT
			placement.border = SideIndents(4)
		}
	}

	private fun onClick(value: Boolean) {
		if (value) {
			open()
		} else {
			close()
		}
	}

	private fun open() {
		if (isOpened) return
		isOpened = true
		toggle.switch.value = true
//		checkRoot()
		node.root += screen
		updateMenu()

		closeThenOutTouch()
	}

	private fun closeThenOutTouch() {
		menu.root.inputs.onTouchDown.then(clickUpSlots) { second ->
			if (Box2d.byDimension(menu.absolutePosition, menu.placement.size).isOutside(second.position) && Box2d.byDimension(node.absolutePosition, node.placement.size).isOutside(second.position)) {
				close()
			}
		}
	}

//	private fun checkRoot() {
//		if (node.root.layout != Layout.SCREEN) throw Error("Expected root.layout == SCREEN. But actual is ${node.root.layout}")
//	}

	private fun close() {
		clickUpSlots.disposeAll()

		if (!isOpened) return
		isOpened = false
		toggle.switch.value = false
		screen.removeSelf()
	}

	override fun applySkin(skin: ComboBoxSkin) {
		updateSelector()
		if (isOpened) {
			updateMenu()
		}
	}
}