package casperix.ui.component.combobox

import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.graphic.BitmapGraphic

data class ComboBoxSkin(val buttonIcon: BitmapGraphic, val selectorSkin:ToggleSkin, val itemSkin:ButtonSkin, val shadow:BitmapGraphic)

