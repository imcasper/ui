package casperix.ui.component.progress

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.core.ConstDimension
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.core.ViewDimension
import casperix.ui.layout.Orientation
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide
import kotlin.math.max

@Deprecated(message = "Use ProgressBarView")
typealias ProgressBar = ProgressBarView

class ProgressBarView(node: UINode = UINode(), side: LayoutSide = LayoutSide.LEFT, initialValue: Double = 0.0) : ViewComponent<ProgressBarSkin>(ProgressBarSkin::class, node) {
	val onValue = StorageSignal(initialValue)
	val onSide = StorageSignal(side)

	constructor(size: Vector2d, side: LayoutSide) : this(UINode(constSize = size), side)

	private val bar = UINode()

	init {
		node += bar

		node.events.onSize.then(components) {
			updateBar()
		}
		onValue.then(components) {
			updateBar()
		}
		onSide.then(components) {
			refreshSkin()
		}

		updateBar()
	}

	private fun updateBar() {
		val skin = skin ?: return
		val maxBarSize = node.placement.size - skin.gap.size
		val side = onSide.value
		val orientation = side.orientation()

		val value = onValue.value

		bar.placement.align = when (side) {
			LayoutSide.LEFT -> LayoutAlign.LEFT
			LayoutSide.RIGHT -> LayoutAlign.RIGHT
			LayoutSide.TOP -> LayoutAlign.TOP
			LayoutSide.BOTTOM -> LayoutAlign.BOTTOM
		}

		if (orientation == Orientation.HORIZONTAL) {
			bar.placement.sizeMode = SizeMode(ConstDimension(max(2.0, maxBarSize.x * value)), ViewDimension)
		} else {
			bar.placement.sizeMode = SizeMode(ViewDimension, ConstDimension(max(2.0, maxBarSize.y * value)))
		}
	}

	override fun applySkin(skin: ProgressBarSkin) {
		refreshSkin()
	}

	private fun refreshSkin() {
		val side = onSide.value
		val orientation = side.orientation()
		val skin = skin ?: return

		val sides = if (orientation == Orientation.HORIZONTAL) skin.horizontal else skin.vertical

		node.graphic = sides.back
		bar.graphic = sides.bar
		bar.placement.gap = skin.gap
	}

}