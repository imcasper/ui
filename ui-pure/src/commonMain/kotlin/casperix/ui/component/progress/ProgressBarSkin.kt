package casperix.ui.component.progress

import casperix.ui.core.SideIndents
import casperix.ui.graphic.Graphic

data class ProgressBarSkin(
	val horizontal: SidedProgressBarSkin,
	val vertical: SidedProgressBarSkin,
	val gap: SideIndents,
)

data class SidedProgressBarSkin(
	val back: Graphic,
	val bar: Graphic,
)