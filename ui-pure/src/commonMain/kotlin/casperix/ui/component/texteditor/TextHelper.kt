package casperix.ui.component.texteditor

import casperix.math.vector.Vector2d
import casperix.ui.component.text.TextView
import casperix.ui.engine.UIManager
import casperix.ui.font.Font
import casperix.ui.engine.TextLayout
import casperix.ui.layout.Align

object TextHelper {
	private class IndexPoint(val index: Int, val center: Vector2d)

	fun getFontInfo(textComponent: TextView): Font? {
		return textComponent.skin?.font
	}

	fun getTextLayout(textComponent: TextView, engine: UIManager): TextLayout? {
		val skin = textComponent.skin ?: return null
		return engine.drawer.layout(skin.font, textComponent.text, textComponent.node.placement.size, Align.MIN, Align.MIN, textComponent.wordWrap)
	}

	fun getNearSymbol(textComponent: TextView, engine: UIManager, localPosition: Vector2d): Int? {
		val layout = getTextLayout(textComponent, engine) ?: return null

		val points = layout.glyphs.flatMap { glyph ->
			val left = glyph.position + glyph.offset
			val right = left + Vector2d(glyph.size.x)

			listOf(
				IndexPoint(glyph.symbolIndex, left),
				IndexPoint(glyph.symbolIndex + 1, right)
			)
		}

		var bestDest = Double.MAX_VALUE
		var bestIndex: Int? = null
		points.forEach { point ->
			val dest = (point.center - localPosition).length()
			if (dest < bestDest) {
				bestDest = dest
				bestIndex = point.index
			}
		}
		return bestIndex
	}
}