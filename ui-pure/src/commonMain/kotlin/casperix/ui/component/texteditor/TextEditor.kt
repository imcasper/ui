package casperix.ui.component.texteditor

import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.component.text.TextView
import casperix.ui.component.text.TextSkin
import casperix.ui.core.*
import casperix.ui.input.TouchCatcherFactory

class TextEditorSkin(val input: TextInputSkin, val helper: TextSkin) 

class TextEditor(preferredWidth:Double?, defaultText: String, val helperText: String, node: UINode = UINode(), onInput: ((String) -> Unit)? = null) : ViewComponent<TextEditorSkin>(TextEditorSkin::class, node) {
	val receiverNode = UINode()
	val logic = TextInputLogic(TextView(defaultText, false, false), receiverNode)
	val helperComponent = TextView(helperText)
	val inputComponent = TextInput(logic, receiverNode)

	val text get() = inputComponent.logic.onText.value

	init {
		if (node.name == null) node.name = "textEditor"
		if (preferredWidth != null) {
			setPreferredWidth(preferredWidth)
		} else {
			setMaxWidth()
		}
		helperComponent.node.touchFilterBuilder = TouchCatcherFactory::never

		node += inputComponent
		node += UINode().apply {
			this += helperComponent
		}

		inputComponent.logic.onText.then(components) {
			refresh()
		}

		onInput?.let { handler ->
			inputComponent.logic.onEnter.then(components) {
				handler(it)
			}
		}
	}

	fun setPreferredWidth(value:Double) {
		inputComponent.node.placement.sizeMode = SizeMode(ConstDimension(value), ChildrenDimension)
	}

	fun setMaxWidth() {
		inputComponent.node.placement.sizeMode = SizeMode(MaxChildrenOrViewDimension, ChildrenDimension)
	}

	private fun refresh() {
		val skin = skin ?: return
		if (text.isEmpty()) {
			helperComponent.node.properties.set(skin.helper)
			helperComponent.text = helperText
		} else {
			helperComponent.node.properties.set(skin.input)
			helperComponent.text = ""
		}
	}

	override fun applySkin(skin: TextEditorSkin) {
		refresh()
	}
}