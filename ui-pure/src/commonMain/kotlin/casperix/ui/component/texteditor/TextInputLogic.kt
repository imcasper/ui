package casperix.ui.component.texteditor

import casperix.input.KeyButton
import casperix.math.vector.Vector2d
import casperix.misc.clamp
import casperix.misc.disposeAll
import casperix.misc.mutableDisposableListOf
import casperix.misc.sliceSafe
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.component.text.TextView
import casperix.ui.core.CursorTypes
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.input.CommonNodeEvent
import casperix.ui.input.TouchFocused

class TextInputLogic(val textComponent: TextView, inputReceiver: UINode, var allowedSymbols: String? = null, var prohibitedSymbols: String? = null, var maxLength: Int? = null) : UIComponent(inputReceiver) {
	private val activeListeners = mutableDisposableListOf()

	val onCursor = StorageSignal(0)
	val onActive = StorageSignal(false)
	val onText = StorageSignal(textComponent.text)
	val onEnter = Signal<String>()

	val text get() = onText.value

	companion object {
		private val BACKSPACE = 8.toChar()
		private val CARRIAGE_RETURN = '\r'
		private val NEWLINE = '\n'
		private val TAB = '\t'
		private val DELETE = 127.toChar()
		private val SPECIAL_SYMBOLS = listOf(BACKSPACE, CARRIAGE_RETURN, NEWLINE, TAB, DELETE)

	}

	private var isCtrl = false
	private var isShift = false

	init {
		LongKeyRepeater(node)

		node.inputs.onTouchDown.then(components) {
			val textEngine = engine ?: return@then
			val textComponent = textComponent
			val nextIndex = TextHelper.getNearSymbol(textComponent, textEngine, it.position - node.absolutePosition)

			if (nextIndex != null) {
				onActive.set(true)
				onCursor.set(nextIndex)
			}
		}

		node.inputs.onTouchFocused.then(components) { touch ->
			onActive.set(touch != null)
		}

		node.inputs.onMouseFocused.then(components) {
			setFocused(it != null)
		}
		onActive.then(components, ::setActive)
	}

	private fun setFocused(focused: Boolean) {
		if (focused) {
			CommonNodeEvent.onCursor.set(CursorTypes.TEXT_CURSOR)
		} else {
			CommonNodeEvent.onCursor.set(CursorTypes.DEFAULT_CURSOR)
		}
	}

	private fun setActive(active: Boolean) {
		if (active) {
			node.inputs.onKeyUp.then(activeListeners) {
				when (it.button) {
					KeyButton.CONTROL_LEFT, KeyButton.CONTROL_RIGHT -> {
						isCtrl = false
					}

					KeyButton.SHIFT_LEFT, KeyButton.SHIFT_RIGHT -> {
						isShift = false
					}

					else -> {

					}
				}
			}

			node.inputs.onKeyDown.then(activeListeners) { event ->
				specialAction(event.button)
				event.captured = true
			}

			node.inputs.onKeyUp.then(activeListeners) { event ->
				event.captured = true
			}

			node.inputs.onKeyTyped.then(activeListeners) { event ->
				if (!SPECIAL_SYMBOLS.contains(event.char)) {
					input(event.char.toString())
					event.captured = true
				}
			}
		} else {
			activeListeners.disposeAll()
		}
	}

	private fun specialAction(button: KeyButton) {
		val cursorIndex = onCursor.value

		when (button) {
			KeyButton.CONTROL_LEFT, KeyButton.CONTROL_RIGHT -> {
				isCtrl = true
			}

			KeyButton.SHIFT_LEFT, KeyButton.SHIFT_RIGHT -> {
				isShift = true
			}

			KeyButton.C -> {
				if (isCtrl) {
					engine?.clipboard?.setContent(text)
				}
			}

			KeyButton.INSERT -> {
				if (isCtrl) {
					engine?.clipboard?.setContent(text)
				} else if (isShift) {
					engine?.clipboard?.getContent()?.let {
						input(it)
					}
				}
			}

			KeyButton.V -> {
				if (isCtrl) {
					engine?.clipboard?.getContent()?.let {
						input(it)
					}
				}
			}

			KeyButton.LEFT -> {
				setCursorIndex(cursorIndex - 1)
			}

			KeyButton.RIGHT -> {
				setCursorIndex(cursorIndex + 1)
			}

			KeyButton.ESCAPE -> {
				onActive.set(false)
			}

			KeyButton.ENTER, KeyButton.NUMPAD_ENTER -> {
				onEnter.set(text)
			}

			KeyButton.BACKSPACE -> {
				removeBackward()
			}

			KeyButton.FORWARD_DELETE -> {
				removeForward()
			}

			else -> {

			}
		}
	}

	private fun filterText(inputSymbols: String): String {
		val allowedSymbols = allowedSymbols
		val prohibitedSymbols = prohibitedSymbols

		return if (allowedSymbols == null && prohibitedSymbols == null) {
			inputSymbols
		} else {
			inputSymbols.filter {
				(allowedSymbols != null && allowedSymbols.contains(it)) || (prohibitedSymbols != null && !prohibitedSymbols.contains(it))
			}
		}
	}

	private fun input(inputSymbols: String) {
		val symbols = filterText(inputSymbols)
		if (symbols.isEmpty()) return

		val cursorIndex = onCursor.value
		val lastText = text
		var nextText = lastText.sliceSafe(0 until cursorIndex) + symbols + lastText.sliceSafe(cursorIndex..lastText.length)

		maxLength?.let {
			nextText = nextText.take(it)
		}

		onText.value = nextText

		setCursorIndex(cursorIndex + symbols.length)
	}

	private fun removeBackward() {
		val cursorIndex = onCursor.value
		val lastText = text
		if (cursorIndex <= 0) return
		onText.value = lastText.sliceSafe(0 until cursorIndex - 1) + lastText.sliceSafe(cursorIndex..lastText.length)
		setCursorIndex(cursorIndex - 1)
	}

	private fun removeForward() {
		val cursorIndex = onCursor.value
		val lastText = text
		if (cursorIndex >= lastText.length) return
		onText.value = lastText.sliceSafe(0 until cursorIndex) + lastText.sliceSafe(cursorIndex + 1..lastText.length)
	}

	private fun setCursorIndex(index: Int) {
		onCursor.value = index.clamp(0, text.length + 1)
	}

	fun makeFocused() {
		val temp = mutableDisposableListOf()
		node.events.nextFrame.then(temp) {
			temp.disposeAll()
			if (node.inputs.onTouchFocused.value != null) return@then
			node.inputs.onTouchFocused.set(TouchFocused(node, Vector2d.ZERO))
		}
	}

}