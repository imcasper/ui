package casperix.ui.component

import casperix.ui.font.Font
import casperix.ui.graphic.Graphic

class DebugSkin(val selfArea: Graphic, val borderArea: Graphic, val gapArea: Graphic, val childrenArea: Graphic, val font: Font)