package casperix.ui.component.chart

import casperix.math.vector.Vector2d
import casperix.ui.component.text.TextSkin

class ChartCanvasTransform(val textSkin: TextSkin, val boxSettings: ChartBoxSettings, val nodePosition: Vector2d, val nodeSize: Vector2d) {
	/** На сколько пикселей растягивать координаты графика	 */
	val scale = nodeSize / boxSettings.sourceArea.dimension * (if (boxSettings.yDown) Vector2d.ONE else Vector2d(1.0, -1.0))

	/** Центр выводимой области в координатах графика	 */
	val chartCenter = boxSettings.sourceArea.center
	val chartSize = boxSettings.sourceArea.dimension

	val nodeCenter = nodePosition + nodeSize / 2.0

	fun chartToScreen(chartPosition: Vector2d): Vector2d {
		return nodeCenter + scale * (chartPosition - chartCenter)
	}
}