package casperix.ui.component.chart

data class ChartData(val name: String, val chartSettings: ChartSettings, val source: ChartSource)