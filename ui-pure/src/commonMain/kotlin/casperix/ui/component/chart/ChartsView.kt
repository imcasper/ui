package casperix.ui.component.chart

import casperix.math.color.Color4d
import casperix.math.vector.Vector2d
import casperix.signals.then
import casperix.ui.component.text.TextView
import casperix.ui.component.text.TextSkin
import casperix.ui.core.SideIndents
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.graphic.ColoredGraphic
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.Layout

class ChartsView(settings: ChartBoxSettings = ChartBoxSettings(), node: UINode = UINode()) : UIComponent(node) {
	private val legendList = UINode()
	private val canvas = UINode()
	private val charts = mutableListOf<ChartData>()

	val textSkin: TextSkin? get() = node.properties.get(TextSkin::class)

	var settings: ChartBoxSettings = settings
		set(value) {
			field = value
			updateLayout()
		}


	init {
//		canvas.clippingContent = true

		node.events.propertyChanged.then(components) {
			updateLayout()
		}
		node.events.onParent.then(components) {
			updateCharts()
		}
	}

	private fun updateLayout() {
		legendList.layout = Layout.VERTICAL
		canvas.placement.gap = SideIndents(settings.axisLabelSize.x, settings.axisLabelSize.y)

		DividerLayout(node, legendList, canvas, settings.legendSide)

		updateCharts()
	}

	private fun updateCharts() {
		val textSkin = textSkin ?: return

		loadSymbols()

		canvas.children.clear()
		legendList.children.clear()

//		val aligner = Aligner(SideIndents(settings.axisLabelSize.x, settings.axisLabelSize.y))
		canvas += UINode(graphic = ChartCanvasDrawer(textSkin, settings))

		charts.forEach {
			val chartSettings = it.chartSettings
			val legendColor = chartSettings.lineSettings?.color ?: chartSettings.pointSettings?.color ?: Color4d.ONE
			val source = it.source

			val graphic = if (source is LineChartSource) {
				LineChartGraphic(textSkin, source, settings, chartSettings)
			} else {
				null
			}

			createLegend(legendList, it.name, legendColor)
//		val aligner = Aligner(SideIndents(settings.axisLabelSize.x, settings.axisLabelSize.y))
			canvas += UINode(graphic = graphic)
		}

	}

	private fun loadSymbols() {
		val engine = engine ?: return
		val textSkin = textSkin ?: return

		val transform = ChartCanvasTransform(textSkin, settings, Vector2d.ZERO, canvas.placement.size)
		val scheme = ChartCanvasScheme(transform)
		val fontSource = textSkin.font.source

		val text = scheme.labels.map {
			it.text
		}.joinToString("")

		engine.supplier.fontFromSource(fontSource, text)
	}

	private fun createLegend(root: UINode, legend: String, color: Color4d) {
		val node = UINode(layout = Layout.LEFT)
		node += UINode(graphic = ColoredGraphic(color), constSize = settings.legendMarkerSize)
		node += TextView(legend)
		root += node
	}

	fun clearAllCharts() {
		charts.clear()
		updateCharts()
	}

	fun addChart(name: String, chartSettings: ChartSettings, source: LineChartSource) {
		removeChart(source)
		addChart(ChartData(name, chartSettings, source))

	}

	fun addChart(chart: ChartData) {
		charts += chart
		updateCharts()
	}

	fun removeChart(source: ChartSource) {
		if (charts.removeAll { it.source == source }) {
			updateCharts()
		}
	}

}