package casperix.ui.component.chart

import casperix.math.vector.Vector2d
import casperix.math.color.toColor
import casperix.ui.component.text.TextSkin
import casperix.ui.engine.Drawer
import casperix.ui.graphic.Graphic
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Orientation


class ChartCanvasDrawer(val textSkin: TextSkin, val settings: ChartBoxSettings) : Graphic {

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		if (!size.greater(Vector2d.ZERO)) return
		if (!settings.sourceArea.dimension.greater(Vector2d.ZERO)) return

		val transform = ChartCanvasTransform(textSkin, settings, position, size)
		val scheme = ChartCanvasScheme(transform)
		val gridLineSettings = settings.gridSettings.lineSettings
		val xLineSettings = settings.xAxis.line
		val yLineSettings = settings.yAxis.line


		//	draw grid
		scheme.grids.forEach {
			drawer.drawLine(it.line, gridLineSettings.color, gridLineSettings.thick)
		}

		val xPointSize = settings.xAxis.markerPointSize
		if (xPointSize != null) {
			scheme.grids.forEach {
				if (it.orientation == Orientation.VERTICAL) {
					val halfSize = Vector2d(0.0, xPointSize / 2.0)
					drawer.drawLine(it.line.v0 - halfSize, it.line.v0 + halfSize, xLineSettings.color, xPointSize / 2.0)
				}
			}
		}
		val yPointSize = settings.yAxis.markerPointSize
		if (yPointSize != null) {
			scheme.grids.forEach {
				if (it.orientation == Orientation.HORIZONTAL) {
					val halfSize = Vector2d(yPointSize / 2.0, 0.0)
					drawer.drawLine(it.line.v0 - halfSize, it.line.v0 + halfSize, yLineSettings.color, yPointSize / 2.0)
				}
			}
		}

		scheme.labels.forEach {
			val graphic = TextGraphic(it.text, textSkin.font, it.color.toColor())
			drawer.drawText(graphic, it.center - it.size / 2.0, it.size)
		}

		drawer.drawLine(position + size.yAxis, position + size, xLineSettings.color, xLineSettings.thick)
		drawer.drawLine(position, position + size.xAxis, gridLineSettings.color, gridLineSettings.thick)

		drawer.drawLine(position + size.yAxis, position, yLineSettings.color, yLineSettings.thick)
		drawer.drawLine(position + size.xAxis, position + size, gridLineSettings.color, gridLineSettings.thick)
	}

}