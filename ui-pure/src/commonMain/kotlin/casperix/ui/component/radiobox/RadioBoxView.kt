package casperix.ui.component.radiobox

import casperix.signals.then
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.UIComponent

class RadioBoxSkin(val custom: ToggleSkin) 

class RadioBoxView(val toggle: ToggleView) : UIComponent(toggle.node) {
	val skin: RadioBoxSkin? get() = node.properties.get(RadioBoxSkin::class)

	init {
		if (node.name == null) node.name = "radio"
		node.events.propertyChanged.then(components) {
			update()
		}
	}

	private fun update() {
		val skin = skin ?: return
		toggle.node.properties.set(skin.custom)
	}
}


