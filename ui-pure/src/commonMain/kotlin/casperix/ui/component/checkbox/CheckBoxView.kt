package casperix.ui.component.checkbox

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.ViewComponent
import casperix.ui.component.button.ButtonLogic
import casperix.ui.component.button.LinkView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleLogic
import casperix.ui.core.UINode
import casperix.ui.layout.Layout

class CheckBoxView(checkSize: Vector2d, signal: StorageSignal<Boolean>) : ViewComponent<CheckBoxSkin>(CheckBoxSkin::class, UINode()) {
	val logic = ToggleLogic(ButtonLogic(node, {}), signal)
	val toggle = ToggleView(logic, UINode(checkSize))
	val switch = logic.switch

	private val content = UINode()

	init {
		if (node.name == null) node.name = "checkBox"
		node.layout = Layout.RIGHT
		node += toggle
		node += content
	}

	fun setLabel(value: String) {
		content.children.clear()
		content += LinkView(value, logic.button, UINode())
	}

	override fun applySkin(skin: CheckBoxSkin) {
		toggle.skin = skin.custom
	}

	constructor(label: String?, checkBoxSize: Vector2d, first: Boolean, action: (Boolean) -> Unit)
			: this(checkBoxSize, StorageSignal(first)) {
		logic.switch.then { action(it) }
		label?.let { setLabel(it) }
	}

	constructor(label: String?, checkBoxSize: Vector2d, signal: StorageSignal<Boolean>)
			: this(checkBoxSize, signal) {
		label?.let { setLabel(it) }
	}
}