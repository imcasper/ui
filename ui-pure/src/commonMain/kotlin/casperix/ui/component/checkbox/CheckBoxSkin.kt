package casperix.ui.component.checkbox

import casperix.ui.component.toggle.ToggleSkin

data class CheckBoxSkin(val custom: ToggleSkin)