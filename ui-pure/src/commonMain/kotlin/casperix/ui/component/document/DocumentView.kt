package casperix.ui.component.document

import casperix.signals.concrete.EmptySignal
import casperix.signals.then
import casperix.ui.component.ViewComponent
import casperix.ui.core.UINode

/**
 *
 * 	val document = Document("It is \"Document\"\n")
 * 	document.text += "Color: [color:FF0000:test/].\n"
 * 	document.text += "Text size: [size:36:test/]\n"
 * 	document.text += "Clickable: [action:some action:click for some action]\n"
 * 	document.text += "Text font: [font:fonts/Verdana.ttf:OTHER FONT/]\n"
 * 	document.text += " Small icon: [image:icon.png]\n"
 * 	document.text += " Resized image: [image:test.png:128:64]\n"
 * 	...
 * 	val documentSkin:DocumentSkin
 * 	documentSkin.customActions["some action"] = { document.text += "\nsome action generated" }
 *
 */
class DocumentView(value: String, node: UINode = UINode()) : ViewComponent<DocumentSkin>(DocumentSkin::class, node) {
	val onChanged = EmptySignal()

	var text: String = value

	val content = UINode()
	private var formatted: String = ""

	init {
		if (node.name == null) node.name = "document"
		content.clippingContent = true
		node += content
		content.layout = null

		onChanged.then(components) {
			update()
		}
		content.events.onSize.then(components) {
			update()
		}
		node.events.propertyChanged.then(components) {
			update()
		}
		node.events.nextFrame.then(components) {
			if (formatted != text) {
				formatted = text
				update()
			}
		}
	}

	private fun updateFont() {
		val engine = engine ?: return
		val skin = skin ?: return
		val fontSource = skin.textSkin.font.source

		engine.supplier.fontFromSource( fontSource, formatted)
	}

	private fun update() {
		val engine = engine ?: return
		val skin = skin ?: return
		val size = content.placement.size

		updateFont()

		val elements = DocumentParser.parse(formatted, skin)
		val nodes = DocumentNodeFactory.getNodes(engine, skin, elements, size)

		node.graphic = skin.back

		content.children.clear()
		content.children.addAll(nodes)
	}

	override fun applySkin(skin: DocumentSkin) {
		update()
	}

}

