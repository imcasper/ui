package casperix.ui.component.document

import casperix.math.vector.Vector2d
import casperix.ui.component.button.LinkView
import casperix.ui.component.document.DocumentParser.parseFileName
import casperix.ui.component.bitmap.BitmapView
import casperix.ui.component.text.TextView
import casperix.ui.component.text.TextSkin
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.engine.UIManager
import casperix.ui.font.Font
import casperix.ui.engine.Supplier
import casperix.ui.font.SymbolWidthMode
import casperix.ui.graphic.TextGraphic
import casperix.ui.layout.Align
import casperix.ui.source.FontLink

class DocumentNodeFactory(engine: UIManager, val skin: DocumentSkin, elements: List<Element>, val size: Vector2d) {

	companion object {


		fun getFont(supplier: Supplier, it: TextElement, textSkin: TextSkin): Font {
			val fontLink = textSkin.font.source as? FontLink ?: return textSkin.font

			val fontFile = if (it.fontName != null) {
				parseFileName(it.fontName)
			} else {
				fontLink.file
			}

			val fontSize = it.fontSize ?: fontLink.size
			val font = supplier.fontFromFile(fontFile, fontSize)
			return font
		}

		fun getNodes(engine: UIManager, skin: DocumentSkin, elements: List<Element>, size: Vector2d): List<UINode> {
			return DocumentNodeFactory(engine, skin, elements, size).output
		}
	}

	class ItemInfo(val offset: Vector2d, val node: UINode)


	var line = mutableListOf<ItemInfo>()
	val output = mutableListOf<UINode>()
	val drawer = engine.drawer
	val supplier = engine.supplier

	var offset = Vector2d.ZERO
	var maxSizeForLine = Vector2d.ZERO
	var isFirstRow = false
	var isFirstColumn = false
	var hasLineEndSymbol = false

	init {
		elements.forEach { pushElement(it) }
		popLine()
	}

	private fun pushElement(element: Element) {
		isFirstRow = offset.y == 0.0
		isFirstColumn = offset.x == 0.0

		if (element is ActionElement) {
			val size = skin.textSkin.font.calculateBounds(element.text, Vector2d(Double.MAX_VALUE), true, SymbolWidthMode.DEFAULT)

			val action = LinkView(element.text, skin.getAction(element.action))
			action.node.placement.sizeMode = SizeMode.const(size)
			pushItem(size, action.node)
		} else if (element is ImageElement) {
			val file = parseFileName(element.source)
			val bitmap = supplier.imageStretch(file)
			pushItem(element.size, BitmapView(bitmap, element.size).node)
		} else if (element is TextElement) {
			val font = getFont(supplier, element, skin.textSkin)
			maxSizeForLine = maxSizeForLine.upper(Vector2d(0.0, font.metrics.lineHeight))

			val isNextLine = element.text == "\n"
			if (isNextLine) {
				nextLine()
				hasLineEndSymbol = true
				return
			}

			val isWhitespace = element.text.length == 1 && element.text.first().isWhitespace()
			if (isWhitespace && !isFirstRow && isFirstColumn && !hasLineEndSymbol) {
				return
			}

			val graphic = TextGraphic(element.text, font, element.color ?: skin.textSkin.color, false, Align.MIN, Align.MIN)
			val itemSize = graphic.font.calculateBounds(graphic)

//			val layout = drawer.layout(font, element.text, Vector2d(Double.POSITIVE_INFINITY), Align.MIN, Align.MIN, false)
//			val layoutSize = layout.size

			val node = if (isWhitespace) {
				null
			} else {
				val component = TextView(element.text, itemSize)
				val customSkin = skin.textSkin.copy(
					color = element.color ?: skin.textSkin.color,
					font = getFont(supplier, element, skin.textSkin)
				)
				if (customSkin != skin.textSkin) {
					component.node.properties.set(customSkin)
				}
				component.node
			}
			pushItem(itemSize, node)
		} else {
			throw Error("Unsupported element: $element")

		}
	}

	private fun popLine() {
		line.forEach { item ->
			val itemSize = item.node.placement.getBound().clientSize
			item.node.placement.setArea(item.offset + Vector2d(0.0, (maxSizeForLine.y - itemSize.y) / 2.0).round(), itemSize)
			output += item.node
		}
		line.clear()
	}

	private fun nextLine() {
		popLine()
		offset = Vector2d(0.0, offset.y + maxSizeForLine.y)
		maxSizeForLine = Vector2d.ZERO
	}

	private fun pushItem(itemSize: Vector2d, itemNode: UINode?) {
		if (itemSize.x + offset.x > size.x && !isFirstColumn) {
			nextLine()
			hasLineEndSymbol = false
		}

		val itemOffset = offset

		maxSizeForLine = maxSizeForLine.upper(itemSize)
		offset += itemSize.xAxis

		if (itemNode != null) {
			line += ItemInfo(itemOffset, itemNode)
		}
	}


}