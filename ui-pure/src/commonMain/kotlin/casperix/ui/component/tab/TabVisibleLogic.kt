package casperix.ui.component.tab

import casperix.signals.then
import casperix.ui.component.VisibleLogic
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class TabVisibleLogic(val tab: AbstractTab, val buttons: UINode, val container: UINode) : UIComponent(tab.button) {
	init {
		buttons.children += tab.button

		val visibleLogic = VisibleLogic(container, tab.content)
		tab.switch.then(components) {
			visibleLogic.onVisible.set(it)
		}
	}

	override fun dispose() {
		super.dispose()
		buttons -= tab.button
		container -= tab.content
	}
}