package casperix.ui.component.timer

import casperix.ui.core.UIComponent
import casperix.ui.core.UINode


class TimerLogic(node: UINode, val maxActionAmount: Int?, val delay: TimerCondition?, val repeat:TimerCondition?, val action: (TimerLogic) -> Unit) : UIComponent(node) {
	enum class TimerPhase {
		WAIT,
		REPEAT,
		COMPLETE,
	}

	private val slot = node.events.nextFrame.then(::update)
	private val checkActionAmount = maxActionAmount != null
	private var remainsActionAmount = maxActionAmount ?: 0
	var phase = TimerPhase.WAIT; private set

	init {
		if (maxActionAmount != null && maxActionAmount <= 0) {
			slot.dispose()
		}
	}

	override fun dispose() {
		phase = TimerPhase.COMPLETE
		slot.dispose()
		super.dispose()
	}

	private fun update(tick: Double) {
		when(phase) {
			TimerPhase.WAIT->{
				if (delay != null && !delay.check(tick)) return
				phase = TimerPhase.REPEAT
			}
			TimerPhase.REPEAT->{
				if (repeat != null && !repeat.check(tick)) return
			}
			TimerPhase.COMPLETE ->{
				slot.dispose()
				return
			}
		}

		action(this)

		if (checkActionAmount) {
			if (--remainsActionAmount <= 0) {
				phase = TimerPhase.COMPLETE
				slot.dispose()
			}
		}
	}

	companion object {
		fun timeInterval(node: UINode, interval: Double, action: (TimerLogic) -> Unit): TimerLogic {
			return TimerLogic(node, null, null, TimeStepCondition(interval), action)
		}

		fun timeDelayAndInterval(node: UINode, delay: Double, interval: Double, action: (TimerLogic) -> Unit): TimerLogic {
			return TimerLogic(node, null, TimeStepCondition(delay), TimeStepCondition(interval), action)
		}

		fun frameInterval(node: UINode, interval: UInt, action: (TimerLogic) -> Unit): TimerLogic {
			return TimerLogic(node, null, null, FrameStepCondition(interval), action)
		}

		fun afterTime(node: UINode, skipInterval: Double, action: (TimerLogic) -> Unit): TimerLogic {
			return TimerLogic(node, 1, TimeStepCondition(skipInterval), null) {
				action(it)
				it.dispose()
			}
		}

		fun afterFrame(node: UINode, skipInterval: UInt, action: (TimerLogic) -> Unit): TimerLogic {
			return TimerLogic(node, 1, FrameStepCondition(skipInterval), null){
				action(it)
				it.dispose()
			}
		}
	}
}