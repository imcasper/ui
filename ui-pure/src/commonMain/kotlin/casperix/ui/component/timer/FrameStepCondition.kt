package casperix.ui.component.timer

class FrameStepCondition(val interval:UInt) : TimerCondition {
	private var remainsFrames = interval

	init {
		if (interval == 0u) throw Error("Skip frames must be positive amount")
	}

	override fun check(tick: Double): Boolean {
		remainsFrames--
		return if (remainsFrames ==0u) {
			remainsFrames += interval
			true
		} else {
			false
		}
	}
}