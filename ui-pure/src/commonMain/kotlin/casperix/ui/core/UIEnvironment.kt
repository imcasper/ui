package casperix.ui.core

import casperix.ui.attribute.AttributeStorage

/**
 * 	Общие для всего интерфейса данные. Например: скины элементов.
 */
object UIEnvironment {
	val attributes = AttributeStorage()
}