package casperix.ui.core

import casperix.app.window.Cursor
import casperix.app.window.SystemCursor

object CursorTypes {
	var HAND_CURSOR:Cursor = SystemCursor.HAND
	var DEFAULT_CURSOR:Cursor = SystemCursor.DEFAULT
	var TEXT_CURSOR:Cursor = SystemCursor.TEXT
}