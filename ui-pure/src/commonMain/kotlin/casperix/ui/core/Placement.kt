package casperix.ui.core

import casperix.math.vector.Vector2d
import casperix.misc.max
import casperix.misc.min
import casperix.ui.layout.Bound
import casperix.ui.layout.LayoutTarget
import casperix.ui.type.LayoutAlign


/**
 * 	Правила размещения элемента внутри доступной области.
 * 	Доступная область определяется родительским элементом.
 * 	Для корневого элемента -- весь размер окна приложения (вью-порт)
 */
class Placement(val node: UINode) : LayoutTarget {
	init {
		node.events.onPosition.then {
			if (!position.isFinite()) throw Error("Invalid position: $position")
		}
		node.events.onSize.then {
			if (!size.isFinite() || !size.greaterOrEq(Vector2d.ZERO)) throw Error("Invalid size: $size")
		}
	}

	/**
	 * 	Рекомендованный зазор от соседних элементов
	 * 	Зазоры накладываются (используется максимальный зазор)
	 */
	override var gap by node.events.onBorder.observable(SideIndents.ZERO)

	/**
	 * 	Рекомендованный отступ от соседних элементов
	 * 	Отступы складываются (используется сумма отступов)
	 */
	override var border by node.events.onBorder.observable(SideIndents.ZERO)

	/**
	 * 	Рекомендуемое выравнивание по вертикали и горизонтали
	 */
	override var align by node.events.onAlign.observable(LayoutAlign.CENTER_CENTER)


	/**
	 * 	Правила определения размера элемента
	 */
	override var sizeMode: SizeMode by node.events.onSizeMode.observable(SizeMode.max)

	/**
	 * 	Положение элемента.
	 * 	Вычисляется автоматически.
	 * 	Зависит от `parent.layout`, `sizeMode`, `align`, `border`, `gap`
	 */
	var position: Vector2d by node.events.onPosition.observable(Vector2d.ZERO); private set

	/**
	 * 	Размер элемента.
	 * 	Вычисляется автоматически.
	 * 	Зависит от `parent.layout`, `sizeMode`, `align`, `border`, `gap`
	 */
	var size: Vector2d by node.events.onSize.observable(Vector2d.ZERO); private set


	override fun setArea(position: Vector2d, size: Vector2d) {
		if (!position.isFinite()) throw UINode.Companion.InvalidParentPosition(position)
		if (!size.isFinite() || !size.greaterOrEq(Vector2d.ZERO)) throw UINode.Companion.InvalidParentSize(size)

		val childrenSize = node.layoutExecutor.childrenSize
		val itemSize = calculateSize(size, childrenSize)

		val alignOffsetX = align.horizontal.getPosition(size.x, itemSize.x)
		val alignOffsetY = align.vertical.getPosition(size.y, itemSize.y)

		this.position = (position + Vector2d(alignOffsetX, alignOffsetY)).round()
		this.size = itemSize.upper(Vector2d.ZERO).round()
	}

	/**
	 * 	Минимальный размер необходимый для отображения элемента
	 */
	override fun getBound(): Bound {
		val childrenSize = node.layoutExecutor.childrenSize
		val size = calculateSize(childrenSize, childrenSize)
		return Bound(size, gap, border)
	}

	private fun calculateSize(actualSize: Vector2d, childrenSize: Vector2d): Vector2d {
		return Vector2d(
			calculateSize(sizeMode.width, actualSize.x, childrenSize.x),
			calculateSize(sizeMode.height, actualSize.y, childrenSize.y)
		)
	}

	private fun calculateSize(dimensionMode: DimensionMode, actualSize: Double, childrenSize: Double): Double {
		val value = when (dimensionMode) {
			is ConstDimension -> dimensionMode.value
			is ChildrenDimension -> childrenSize
			is ViewDimension -> actualSize
			is MinChildrenOrViewDimension -> min(actualSize, childrenSize)
			is MaxChildrenOrViewDimension -> max(actualSize, childrenSize)
		}
		if (!value.isFinite()) return 0.0
		return value
	}

	private fun calculateMaxSize(dimensionMode: DimensionMode, actualSize: Double, childrenSize: Double): Double {
		val value = max(actualSize, childrenSize)
		if (!value.isFinite()) return 0.0
		return value
	}
}