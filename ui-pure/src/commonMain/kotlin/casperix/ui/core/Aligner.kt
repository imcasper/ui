package casperix.ui.core

import casperix.ui.layout.Align
import casperix.ui.type.LayoutSide

data class Aligner(val left: Double?, val top: Double?, val right: Double?, val bottom: Double?) {
	constructor(indents: SideIndents) : this(indents.left, indents.top, indents.right, indents.bottom)

	constructor(horizontalAlign: Align, verticalAlign: Align) : this(
		if (horizontalAlign == Align.MIN) 0.0 else null,
		if (verticalAlign == Align.MIN) 0.0 else null,
		if (horizontalAlign == Align.MAX) 0.0 else null,
		if (verticalAlign == Align.MAX) 0.0 else null,
	)

	companion object {
		val EMPTY = Aligner(null, null, null, null)
		val FULL = Aligner(0.0, 0.0, 0.0, 0.0)

		fun bySide(side: LayoutSide): Aligner {
			return when (side) {
				LayoutSide.LEFT -> LEFT
				LayoutSide.RIGHT -> RIGHT
				LayoutSide.TOP -> TOP
				LayoutSide.BOTTOM -> BOTTOM
			}
		}

		val LEFT = FULL.copy(right = null)
		val RIGHT = FULL.copy(left = null)
		val TOP = FULL.copy(bottom = null)
		val BOTTOM = FULL.copy(top = null)

		val LEFT_TOP = EMPTY.copy(left = 0.0, top = 0.0)
		val CENTER_TOP = EMPTY.copy(top = 0.0)
		val RIGHT_TOP = EMPTY.copy(right = 0.0, top = 0.0)

		val LEFT_CENTER = EMPTY.copy(left = 0.0)
		val CENTER_CENTER = EMPTY
		val RIGHT_CENTER = EMPTY.copy(right = 0.0)

		val LEFT_BOTTOM = EMPTY.copy(left = 0.0, bottom = 0.0)
		val CENTER_BOTTOM = EMPTY.copy(bottom = 0.0)
		val RIGHT_BOTTOM = EMPTY.copy(right = 0.0, bottom = 0.0)

	}
}