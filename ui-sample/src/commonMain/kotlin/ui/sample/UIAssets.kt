package ui.sample

import casperix.file.FileReference
import casperix.math.axis_aligned.Box2i
import casperix.math.vector.Vector2i
import casperix.ui.core.SideIndents
import casperix.ui.engine.Supplier
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.Scaling9Slice

class UIAssets(val supplier: Supplier) {

	val redFrame = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("ui/redFrame.png")), Scaling9Slice(SideIndents(4)))
	val greenFrame = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("ui/greenFrame.png")), Scaling9Slice(SideIndents(4)))
	val blueFrame = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("ui/blueFrame.png")), Scaling9Slice(SideIndents(4)))
	val whiteFrame = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("ui/whiteFrame.png")), Scaling9Slice(SideIndents(4)))
	val scale9 = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("scale9.png")), Scaling9Slice(SideIndents(2, 4, 8, 16)))

	val test = supplier.bitmapFromFile(FileReference.classpath("test.png"))
	val testPart = supplier.bitmapFromFile(FileReference.classpath("test.png"), region = Box2i(Vector2i(50, 50), Vector2i(100, 100)))

	val icon = BitmapGraphic(supplier.bitmapFromFile(FileReference.classpath("icon.png")), Scaling9Slice(SideIndents(2)))
}