package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.math.color.setAlpha
import casperix.ui.component.UIMovable
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.engine.Shape
import casperix.ui.graphic.ShapeGraphic
import casperix.ui.input.SmartTouchCatcher
import casperix.ui.input.TouchCatcherFactory

class UIShape(val shape: Shape) : UIComponent(UINode()) {
	val main = UINode()
	val shadow = UINode()

	val mover = UIMovable(node)

	init {
		var max = Vector2d.ZERO
		shape.segments.forEach { segment ->
			segment.triangles.forEach { triangle ->
				triangle.getVertices().forEach {
					max = it.upper(max)
				}
			}
		}

		node.layout = null
		main.layout = null
		shadow.layout = null

		main.placement.setArea(Vector2d.ZERO, max)
		shadow.placement.setArea(Vector2d.ZERO, max)

		node += shadow
		node += main


		shadow.placement.setArea(Vector2d.ZERO, Vector2d(1.0, 3.0))

		node.inputs.onMouseFocused.then {
			updateGraphic()
		}
		mover.dragging.then {
			updateGraphic()
		}

		main.touchFilterBuilder = SmartTouchCatcher::inside
		shadow.touchFilterBuilder = TouchCatcherFactory::never

		updateGraphic()

	}

	private fun updateGraphic() {
		val moving = mover.dragging.value
		val focused = node.inputs.onMouseFocused.value != null

		main.graphic = ShapeGraphic(shape)
	}
}