package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.math.vector.nextVector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.panel.PanelView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.layout.DividerLayout
import casperix.ui.layout.Layout
import ui.sample.Environment

class UIPlayground(val environment: Environment) {
	val tab = ToggleTab(ToggleView("play", environment.style.buttonSize, StorageSignal(false)), UINode())

	private val content = tab.content
	private val canvas = UINode()


	init {

		canvas.layout = null
		canvas.clippingContent = true

		val controlPanel = UINode()
		controlPanel.layout =  Layout.VERTICAL
		PanelView(controlPanel)
		controlPanel += ButtonView("add random triangle", Vector2d(260.0, 40.0)) {
			addRandomTriangle()
		}
		controlPanel += TextView("You can test touch\nin area below.")

		DividerLayout(content, controlPanel, canvas)

		(1..5).forEach {
			addRandomTriangle()
		}
	}

	private fun addRandomTriangle() {
		val shape = UIShape(UIHelper.createRandomShape())
		shape.node.placement.setArea(UIHelper.random.nextVector2d(Vector2d(400.0)), Vector2d.ZERO)
		canvas += shape
	}
}