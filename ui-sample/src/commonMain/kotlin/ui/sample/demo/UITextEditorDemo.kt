package ui.sample.demo

import casperix.misc.sliceSafe
import casperix.input.KeyButton
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.texteditor.TextEditor
import casperix.ui.component.texteditor.TextInput
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.*
import casperix.ui.layout.Layout
import ui.sample.Environment

class UITextEditorDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("editor", environment.style.buttonSize, StorageSignal(false)), UINode(layout = Layout.VERTICAL))
	private val info = TextView("")
	private val infoMaxLines = 30

	private val events = mutableListOf<String>()

	init {
		val editor1 = TextInput("test")
		editor1.node.placement.sizeMode = SizeMode(ConstDimension(300.0), ChildrenDimension)
		tab.content += TextView("Simple input")
		tab.content += editor1

		val editor2 = TextEditor(300.0, "", "<input here...>")
		tab.content += TextView("Improved input")
		tab.content += editor2

		info.text = "Press some key for detail"
		tab.content += info
		tab.content.layout = Layout.VERTICAL


		attachInputPrinter("Simple input", editor1.node)
		attachInputPrinter("Improved input", editor2.node)

		tab.switch.then {
			if (it) {
				editor2.logic.makeFocused()
			}
		}
	}

	private fun attachInputPrinter(name: String, editor: UINode) {
		editor.inputs.onKeyDown.then {
			events.add("$name: KeyDown: ${getButtonName(it.button)}")
			updateInfo()
		}

		editor.inputs.onKeyTyped.then {
			events.add("$name: KeyTyped: \"${it.char}\"")
			updateInfo()
		}
		editor.inputs.onKeyUp.then {
			events.add("$name: KeyUp: ${getButtonName(it.button)}")
			updateInfo()
		}
	}

	private fun getButtonName(button: KeyButton): String {
		return "${button.name} => ${button.code}"
	}

	private fun updateInfo() {
		val output = events.sliceSafe(events.size - infoMaxLines..events.size)
		info.text = output.joinToString("\n")
	}

}