package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.button.ButtonSkin
import casperix.ui.component.combobox.ComboBoxView
import casperix.ui.component.frame.FrameView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.toggle.ToggleSkin
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import ui.sample.Environment

class UISpecialDemo(val environment: Environment) {
	val frameSize = UIHelper.FRAME_SIZE
	val tab = ToggleTab(ToggleView("special", environment.style.buttonSize, StorageSignal(false)), UINode())
	private val info = TextView("")

	init {
		tab.toggle.switch.then {
			if (it) {
				tab.content.children.clear()
				create()
			}
		}
	}

	fun create() {
		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.VERTICAL, 4))
		content += FrameView(frameSize, "output", info)

		content += FrameView(
			frameSize,
			"combo box",
			ComboBoxView(Vector2d(160.0, 40.0), "var1", listOf("var1", "var2", "var3"), { it })
		)
		content += FrameView(
			frameSize,
			"selectable buttons",
			createSelectableButtons()
		)
		content += FrameView(
			frameSize,
			"selectable toggles",
			createSelectableToggles()
		)
	}

	private fun createSelectableButtons(): UINode {
		val main = UINode(layout = Layout.VERTICAL)
		main.setSkin<ButtonSkin>(environment.root.getSkin("selectable")!!)
		main += ButtonView("test", Vector2d(120.0, 40.0), {})
		main += ButtonView("A", Vector2d(120.0, 40.0), {})
		main += ButtonView("some other", Vector2d(120.0, 40.0), {})
		return main
	}

	private fun createSelectableToggles(): UINode {
		val main = UINode(layout = Layout.VERTICAL)
		main.setSkin<ToggleSkin>(environment.root.getSkin("selectable")!!)
		main += ToggleView("test", Vector2d(120.0, 40.0), false, {})
		main += ToggleView("A", Vector2d(120.0, 40.0), false, {})
		main += ToggleView("some other", Vector2d(120.0, 40.0), false, {})
		return main
	}

}