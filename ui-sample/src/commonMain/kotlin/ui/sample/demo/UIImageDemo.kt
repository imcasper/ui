package ui.sample.demo

import casperix.map2d.IntMap2D
import casperix.math.PolarCoordinated
import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.math.geometry.Quad2d
import casperix.math.geometry.Triangle2d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.math.vector.toDecart
import casperix.math.vector.toPolar
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.frame.FrameView
import casperix.ui.component.bitmap.BitmapView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.graphic.*
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import ui.sample.Environment

class UIImageDemo(val environment: Environment) {
	val frameSize = UIHelper.FRAME_SIZE
	val tab = ToggleTab(ToggleView("image", environment.style.buttonSize, StorageSignal(false)), UINode())

	private val generatedImage = environment.engine.supplier.bitmapFromPixels(IntMap2D.createByXY(Vector2i(50)) {
		if (it.x < 10 || it.y < 10 || it.x > 40 || it.y > 40) {
			Color(200, 0, 50, 200).value
		} else {
			Color(50, 0, 200, 200).value
		}
	})

	init {
		tab.toggle.switch.then {
			if (it) {
				tab.content.children.clear()
				create()
			}
		}
	}

	fun create() {
		val imageSize = Vector2d(200.0, 120.0)

		val content = ScrollBoxView.viewport(tab.content, TableLayout(Orientation.VERTICAL, 4))

		environment.assets.apply {
			content += FrameView(frameSize, "image (default) ", BitmapView(test))
			content += FrameView(frameSize, "image (part) ", BitmapView(testPart))
			content += FrameView(frameSize, "image (colored) ", BitmapView(BitmapGraphic(test, color = Color4d(1.0, 0.0, 1.0, 0.8))))
			content += FrameView(frameSize, "image (generated) ", BitmapView(generatedImage))

			content += FrameView(frameSize, "image (flipX) ", BitmapView(BitmapGraphic(test, ScalingStretch(), flipX = true), imageSize))
			content += FrameView(frameSize, "image (flipY) ", BitmapView(BitmapGraphic(test, ScalingStretch(), flipY = true), imageSize))
			content += FrameView(frameSize, "image (scale9) ", BitmapView(scale9, imageSize))
			content += FrameView(frameSize, "image (no scale) ", BitmapView(BitmapGraphic(test, ScalingNone()), true))
			
			content += FrameView(frameSize, "image (adaptive) ", BitmapView(BitmapGraphic(test, ScalingAdaptive()), true))
			content += FrameView(frameSize, "image (stretch) ", BitmapView(BitmapGraphic(test, ScalingStretch()), true))
			content += FrameView(frameSize, "image (uniform) ", BitmapView(BitmapGraphic(test, ScalingUniform(0.5)), true))
			content += FrameView(frameSize, "image (slice) ", BitmapView(BitmapGraphic(test, Scaling9Slice(2.0)), true))

			content += FrameView(frameSize, "use custom shape", createCustomShape())
		}

		content += FrameView(
			frameSize,
			"blue shape", UINode(
				constSize = frameSize,
				graphic =
				ShapeGraphic(
					Color.BLUE.toColor4d(),
					Quad2d(
						Vector2d(10.0, 20.0),
						Vector2d(200.0, 0.0),
						Vector2d(120.0, 90.0),
						Vector2d(0.0, 100.0),
					),
				)
			)
		)

		content += FrameView(
			frameSize,
			"red shape", UINode(
				constSize = frameSize,
				graphic =
				ShapeGraphic(
					Color4d(1.0, 0.0, 0.0, 1.0),
					Triangle2d(
						Vector2d(0.0, 100.0),
						Vector2d(200.0, 300.0),
						Vector2d(200.0, 0.0),
					),
				)
			)
		)
	}

	private fun createCustomShape(): UINode {
		val node = UINode(constSize = frameSize)
		val radius = frameSize.x / 3.0
		val offset = frameSize / 2.0


		val shape = Quad2d(Vector2d(-radius, -radius), Vector2d(-radius, radius), Vector2d(radius, radius), Vector2d(radius, -radius))
		var angle = 0.0

		node.events.nextFrame.then {
			angle += 0.01
			val transformedShape = shape.convert {
				val polar = it.toPolar()
				val decart = PolarCoordinated(polar.range, polar.angle + angle).toDecart()
				Vector2d(offset.x + decart.x, offset.y + decart.y * 0.4)
			}
			node.graphic = QuadBitmapGraphic(environment.assets.test, transformedShape)
		}

		return node
	}


}