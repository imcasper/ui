package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.panel.PanelView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout
import ui.sample.Environment

class UIScrollDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("scroll", environment.style.buttonSize, StorageSignal(false)), UINode())
	private val content = tab.content

	init {
		val scroller = ScrollBoxView(UINode())
		generateContent(scroller.content, TableLayout(Orientation.VERTICAL, 2))
		content += scroller
	}

	private fun generateContent(root: UINode, layout: Layout) {
		root.layout = layout
		for (i in 1..100) {
			val item = UINode(layout = Layout.VERTICAL, constSize = Vector2d(200.0, 100.0))
			item += TextView("test $i")
			PanelView(item)
			root += item
		}
	}

}