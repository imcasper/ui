package ui.sample.demo.layout

import casperix.signals.concrete.StorageSignal
import casperix.math.vector.Vector2d
import casperix.ui.component.button.ButtonView
import casperix.ui.component.checkbox.CheckBoxView
import casperix.ui.component.frame.FrameView
import casperix.ui.component.text.TextView
import casperix.ui.core.*
import casperix.ui.layout.Align
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide
import casperix.ui.component.NumberEditor
import ui.component.SideIndentsEditor
import ui.component.selector.SideSelector
import ui.sample.Environment
import kotlin.random.Random

class UILayoutDemo(val environment: Environment, settings: LayoutSettings, val setupLayout: (UINode, LayoutSettings) -> Unit) : UIComponent(UINode()) {
	private val preferredSize = Vector2d(600.0, 400.0)
	private val buttonSize = environment.style.buttonSize
	private val checkSize = environment.style.checkerSize

	private val buttons = UINode(layout = Layout.VERTICAL)
	private val content = UINode()

	private var randomId = settings.randomId
	private val nodeAmountEditor = NumberEditor.createIntIncrementer("amount", settings.amount)
	private val sideEditor = SideSelector.create("side", buttonSize, LayoutSide.LEFT)
	private val rowLengthEditor = NumberEditor.createIntIncrementer("row length", (settings.rowLength ?: 1))
	private val rowOrientationEditor = CheckBoxView("row horizontal", checkSize, StorageSignal(false))
	private val constSizeEditor = CheckBoxView("const size", checkSize, StorageSignal(settings.layoutConstSize))

	private val borderEditor = SideIndentsEditor("item border", initial = settings.itemBorder)
	private val gapEditor = SideIndentsEditor("item gap", initial = settings.itemGap)

	init {
		node += buttons
		node += content
		node.layout = Layout.HORIZONTAL

		setupControlButtons(settings)
		setupContent()
		generateItems()
	}

	private fun setupContent() {
		content.clippingContent = true
		content.placement.align = LayoutAlign.LEFT_TOP
		content.graphic = environment.assets.whiteFrame
	}

	private fun setupControlButtons(settings: LayoutSettings) {
		buttons.layout = Layout.VERTICAL

		val frame = FrameView("misc", Layout.VERTICAL)
		frame.content += ButtonView("generate", environment.style.buttonSize) { ++randomId; generateItems() }

		frame.content += nodeAmountEditor
		if (settings.rowLength != null) frame.content += rowLengthEditor
		if (settings.rowOrientation != null) frame.content += rowOrientationEditor
		frame.content += constSizeEditor
		buttons += borderEditor
		buttons += gapEditor
		buttons += frame

		if (settings.layoutSide != null) buttons += sideEditor

		nodeAmountEditor.observer.then { generateItems() }
		sideEditor.onSelect.then { updateLayouts() }
		rowOrientationEditor.switch.then { updateLayouts() }
		rowLengthEditor.observer.then { updateLayouts() }
		borderEditor.onValue.then { generateItems() }
		gapEditor.onValue.then { generateItems() }

		constSizeEditor.switch.then {
			if (it) {
				content.placement.sizeMode = SizeMode.const(preferredSize)
			} else {
				content.placement.sizeMode = SizeMode.max
			}
		}
	}

	private fun getCurrentSettings(): LayoutSettings {
		val isHorizontal = rowOrientationEditor.switch.value

		return LayoutSettings(
			nodeAmountEditor.observer.value,
			borderEditor.onValue.value,
			gapEditor.onValue.value,
			constSizeEditor.switch.value,
			sideEditor.onSelect.value,
			if (isHorizontal) Orientation.HORIZONTAL else Orientation.VERTICAL,
			rowLengthEditor.observer.value,
			randomId = randomId,
		)
	}

	private fun updateLayouts() {
		validateSettings()
		setupLayout(content, getCurrentSettings())
	}

	private fun generateItems() {
		content.children.clear()
		validateSettings()
		val settings = getCurrentSettings()

		val random = Random(settings.randomId)
		val graphic = listOf(environment.assets.greenFrame, environment.assets.redFrame, environment.assets.blueFrame).random(random)

		val sizeModeList = listOf(
			SizeMode.const(Vector2d(240.0, 180.0)),
			SizeMode.const(Vector2d(240.0, 120.0)),
			SizeMode(ConstDimension(300.0), MaxChildrenOrViewDimension),
			SizeMode(MaxChildrenOrViewDimension, ConstDimension(300.0)),
			SizeMode(MaxChildrenOrViewDimension, MaxChildrenOrViewDimension),
			SizeMode(ConstDimension(300.0), MinChildrenOrViewDimension),
			SizeMode(MinChildrenOrViewDimension, ConstDimension(300.0)),
			SizeMode(MinChildrenOrViewDimension, MinChildrenOrViewDimension)
		)
		val aligns = listOf(Align.MIN, Align.CENTER, Align.MAX, Align(0.2), Align(0.8))

		for (i in 0 until settings.amount) {
			val sizeMode = sizeModeList.random(random)
			val hAlign = aligns.random(random)
			val vAlign = aligns.random(random)

			val shape = UINode()
			shape.graphic = graphic
			shape.placement.sizeMode = sizeMode
			shape.placement.align = LayoutAlign(hAlign, vAlign)
			shape.placement.border = settings.itemBorder
			shape.placement.gap = settings.itemGap

			val info = TextView(
				"№:" + (i + 1) + "\n" +
						"Sz:" + sizeMode + "\n" +
						"V:" + vAlign + "\n" +
						"H:" + hAlign
			)
			shape += info

			content += shape
		}
		updateLayouts()
	}

	private fun validateSettings() {
		if (nodeAmountEditor.observer.value < 0) nodeAmountEditor.observer.value = 0
		if (rowLengthEditor.observer.value < 1) rowLengthEditor.observer.value = 1
	}

}