package ui.sample.demo.layout

import casperix.ui.core.SideIndents
import casperix.ui.layout.Orientation
import casperix.ui.type.LayoutSide

class LayoutSettings(
	val amount: Int,
	val itemBorder: SideIndents,
	val itemGap: SideIndents,
	val layoutConstSize: Boolean = true,
	val layoutSide: LayoutSide? = null,
	val rowOrientation: Orientation? = null,
	val rowLength: Int? = null,
	val randomId: Int = 0
)