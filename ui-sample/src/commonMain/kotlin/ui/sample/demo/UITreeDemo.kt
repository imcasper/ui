package ui.sample.demo

import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.ui.component.button.ButtonView
import casperix.ui.component.panel.PanelView
import casperix.ui.component.scroll.ScrollBoxView
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.TextView
import casperix.ui.component.toggle.ToggleView
import casperix.ui.component.tree.TreeView
import casperix.ui.component.tree.TreeNode
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import ui.sample.Environment

class UITreeDemo(val environment: Environment) {
	val tab = ToggleTab(ToggleView("tree", environment.style.buttonSize, StorageSignal(false)), UINode())

	init {
		val content = ScrollBoxView.viewport(tab.content, Layout.BOTTOM)
		TreeView(true, createComplexTree(), content)
	}

	private fun createSmallTree(): TreeNode {
		val tree = TreeNode(TextView("independent tree").node, true)
		tree.children += listOf(
			TreeNode(TextView("empty A").node),
			TreeNode(TextView("empty B").node),
		)
		return tree
	}

	private fun createComplexTree(): TreeNode {
		val tree = TreeNode(TextView("root").node, true)
		tree.children += listOf(
			TreeNode(TextView("empty A").node),
			TreeNode(TextView("empty B").node),
			TreeNode(
				TextView("container").node, false, listOf(
					TreeNode(
						TextView("sub container").node, false, listOf(
							TreeNode(TextView("sub sub A").node),
							TreeNode(TextView("sub sub B").node),
						)
					),
					TreeNode(TextView("sub A").node),
				)
			),
			TreeNode(
				TextView("opened container").node, true, listOf(
					TreeNode(ButtonView("you can add any element", preferredSize = Vector2d(350.0, 30.0), onClick = {}).node),
					TreeNode(TextView("even one more... ").node),
					TreeNode(PanelView(TreeView(false, createSmallTree(), UINode()).node).node)
				)
			),
		)
		return tree
	}

}