package ui.component

import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.ui.core.SideIndents
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode

class SideIndentsEditor(title: String, leftName: String = "left", topName: String = "top", rightName: String = "right", bottomName: String = "bottom", initial: SideIndents = SideIndents.ZERO) : UIComponent(UINode()) {
	val onValue = StorageSignal<SideIndents>(initial)

	init {
		val editor = VectorEditor(title, listOf(leftName, topName, rightName, bottomName), listOf(initial.left, initial.top, initial.right, initial.bottom), node)
		editor.onValue.then {
			val left = it.getOrElse(0) { 0.0 }
			val top = it.getOrElse(1) { 0.0 }
			val right = it.getOrElse(2) { 0.0 }
			val bottom = it.getOrElse(3) { 0.0 }
			onValue.set(SideIndents(left, top, right, bottom))
		}
	}
}