package ui.component

import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.component.NumberEditor
import casperix.ui.component.frame.FrameView
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout

class VectorEditor(title: String, names: List<String>, initial: List<Double>, node: UINode = UINode()) : UIComponent(node) {
	val onValue = StorageSignal(initial)

	private val editors = names.mapIndexed { index, name -> NumberEditor.createDoubleIncrementer(name, 1, initial.getOrNull(index) ?: 0.0) }
	private val frame = FrameView()

	init {
		frame.title = title
		frame.content.layout = Layout.BOTTOM
		node += frame

		editors.forEach {
			frame.content += it
			it.observer.then(components) { updateValues() }
		}
	}

	private fun updateValues() {
		val result = editors.map { it.observer.value }
		onValue.set(result)
	}

}

