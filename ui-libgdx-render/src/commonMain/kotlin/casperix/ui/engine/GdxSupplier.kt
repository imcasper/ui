package casperix.ui.engine

import casperix.file.FileReference
import casperix.map2d.IntMap2D
import casperix.math.axis_aligned.Box2i
import casperix.ui.engine.font.GdxFont
import casperix.ui.engine.font.FontSupplier
import casperix.ui.source.*

class GdxSupplier(val yDown: Boolean) : Supplier {
	private val textureSupplier = GdxTextureSupplier()
	private val fontSupplier = FontSupplier(yDown)

	private fun getImage(source: BitmapSource, smooth: Boolean, mipMap: Boolean, region: Box2i?): TextureContainer {
		return textureSupplier.get(source, region, smooth, mipMap)
	}

	private fun removeImage(source: BitmapSource) {
		textureSupplier.remove(source)
	}

	override fun bitmapFromFile(file: FileReference, smooth: Boolean, mipMap: Boolean, region: Box2i?): TextureContainer {
		return getImage(BitmapFile(file), smooth, mipMap, region)
	}

	override fun bitmapFromPixels(map: IntMap2D, smooth: Boolean, mipMap: Boolean, region: Box2i?): TextureContainer {
		return getImage(BitmapData(map), smooth, mipMap, region)
	}

	override fun bitmapFromBytes(encoded: ByteArray, smooth: Boolean, mipMap: Boolean, region: Box2i?): TextureContainer {
		return getImage(BitmapEncoded(encoded), smooth, mipMap, region)
	}

	override fun bitmapRemove(file: FileReference) {
		removeImage(BitmapFile(file))
	}

	override fun bitmapRemove(map: IntMap2D) {
		removeImage(BitmapData(map))
	}

	override fun bitmapRemove(encoded: ByteArray) {
		removeImage(BitmapEncoded(encoded))
	}

	override fun fontFromSource(source: FontSource, symbols: String?): GdxFont {
		return fontSupplier.get(source, (symbols ?: ""))
	}
}