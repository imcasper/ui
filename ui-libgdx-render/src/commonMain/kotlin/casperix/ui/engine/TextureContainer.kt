package casperix.ui.engine

import casperix.math.axis_aligned.Box2i
import casperix.misc.Disposable
import casperix.math.vector.Vector2i
import casperix.math.color.Color
import casperix.ui.source.BitmapSource
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture

class TextureContainer(override val source: BitmapSource, val texture: Texture, override val region:Box2i) : Bitmap, Disposable {
	override val smooth: Boolean = texture.minFilter != Texture.TextureFilter.Nearest || texture.magFilter != Texture.TextureFilter.Nearest

	private var captured:Pixmap? = null

	override fun get(position: Vector2i): Color {
		val pixmap = captured ?: texture.textureData.consumePixmap()
		captured = pixmap

		val globalPosition = region.min + position
		return Color(pixmap.getPixel(globalPosition.x, globalPosition.y))
	}

	override fun dispose() {
		captured?.dispose()
		texture.dispose()
	}
}