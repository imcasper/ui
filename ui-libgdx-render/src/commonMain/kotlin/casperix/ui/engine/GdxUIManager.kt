package casperix.ui.engine

import casperix.app.window.WindowWatcher
import casperix.gdx.geometry.toVector3
import casperix.gdx.geometry.toVector3d
import casperix.gdx.input.Inputs
import casperix.gdx.input.registerInput
import casperix.input.PointerEvent
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.misc.DisposableHolder
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import casperix.ui.core.SizeMode
import casperix.ui.core.UINode
import casperix.ui.input.CommonNodeEvent
import casperix.ui.input.UIInputDispatcher
import casperix.ui.layout.Layout
import casperix.ui.skin.BaseSkinSetup
import casperix.ui.source.FontSource
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL30
import kotlin.concurrent.fixedRateTimer


class GdxUIManager(
    watcher: WindowWatcher,
    autoRegisterInput: Boolean,
    autoClearColor: Boolean,
    defaultFontSize: Int = 14,
    customFontSource: FontSource? = null
) : DisposableHolder(), UIManager {
    private val yDown = true

    override val root = UINode(name = "root", layout = Layout.SCREEN)
    override val supplier = GdxSupplier(yDown)
    override val drawer = GdxDrawer(yDown, supplier)
    override val clipboard = GdxClipboard()

    private val uiInputDispatcher = UIInputDispatcher(root, Inputs())
    val input = uiInputDispatcher.dispatcher

    val onDebugInput = StorageSignal(false)
    val on1FPSMode = StorageSignal(false)
    var skin = BaseSkinSetup(supplier, root.properties, defaultFontSize, customFontSource)

    init {
        onDebugInput.then { UIInputDispatcher.isDebugMode = it }
        watcher.onUpdate.then(components) { update(it) }
        if (autoClearColor) {
            watcher.onPreRender.then(components) { Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT) }
        }
        watcher.onPostRender.then(components) { draw() }
        watcher.onExit.then(components) { dispose() }

        if (autoRegisterInput) {
            registerInput(input as InputProcessor)
        }

        CommonNodeEvent.onCursor.then {
            watcher.onCursor.value = it
        }

        root.properties.set(UIManager::class, this)

        updateViewport()

        on1FPSMode.then { slowMode ->
            Gdx.graphics.isContinuousRendering = !slowMode
        }
        fixedRateTimer(period = 1000L) {
            if (on1FPSMode.value) {
                Gdx.graphics.requestRendering()
            }
        }
    }

    private fun update(tick: Double) {
        updateViewport()
        updateNode(root, tick, Vector2d.ZERO)
    }

    private fun draw() {
        drawer.draw(root)
    }

    @Deprecated(message = "use Camera.project2d")
    fun project(camera: Camera, worldPosition: Vector3d): Vector2d {
        return camera.project(worldPosition.toVector3()).toVector3d().getXY()
    }

    private fun updateViewport() {
        val width = Gdx.graphics.width
        val height = Gdx.graphics.height

        val size = Vector2d(width.toDouble(), height.toDouble())

        root.placement.sizeMode = SizeMode.const(size)
        root.placement.setArea(Vector2d.ZERO, size)
        drawer.setSize(width, height)
    }

    private fun updateNode(node: UINode, tick: Double, parentAbsolutePosition: Vector2d) {
        val absolutePosition = node.placement.position + parentAbsolutePosition

        node.children.forEach {
            updateNode(it, tick, absolutePosition)
        }

        uiInputDispatcher.setFocusIfEmpty(node)

        node.events.nextFrame.set(tick)
    }

    /**
     * 	Simulate pointer event
     */
    fun dispatchTouchEvent(event: PointerEvent, first: UINode): Boolean {
        return uiInputDispatcher.dispatchTouchEvent(event, first)
    }

    override fun dispose() {
        drawer.dispose()
    }

}