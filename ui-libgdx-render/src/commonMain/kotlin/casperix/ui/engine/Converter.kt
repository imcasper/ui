package casperix.ui.engine

import casperix.file.FileReference
import casperix.map2d.IntMap2D
import casperix.math.color.Color
import casperix.misc.toByteBuffer
import casperix.ui.layout.Align
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.Color as GdxColor
import com.badlogic.gdx.utils.Align as GdxAlign

fun Align.toVerticalAlign(): Int {
	if (this == Align.MIN) return GdxAlign.bottom
	if (this == Align.CENTER) return GdxAlign.center
	if (this == Align.MAX) return GdxAlign.top

	return 0
}

fun Align.toHorizontalAlign(): Int {
	if (this == Align.MIN) return GdxAlign.left
	if (this == Align.CENTER) return GdxAlign.center
	if (this == Align.MAX) return GdxAlign.right

	return 0
}

fun Color.toColor(): GdxColor {
	return GdxColor(value)
}


fun IntMap2D.toPixmap(): Pixmap {
	val pixmap = Pixmap(width, height, Pixmap.Format.RGBA8888)
	pixmap.pixels.put(array.toByteBuffer())
	return pixmap
}


fun FileReference.toGdxFile():FileHandle {
	return Gdx.files.internal(path)
}