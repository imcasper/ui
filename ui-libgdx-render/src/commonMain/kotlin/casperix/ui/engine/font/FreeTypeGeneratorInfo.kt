package casperix.ui.engine.font

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

class FreeTypeGeneratorInfo(val generator: FreeTypeFontGenerator, val data: FreeTypeFontGenerator.FreeTypeBitmapFontData)