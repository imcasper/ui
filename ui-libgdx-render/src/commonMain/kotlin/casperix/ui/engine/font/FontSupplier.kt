package casperix.ui.engine.font

import casperix.misc.Disposable
import casperix.ui.source.FontLink
import casperix.ui.source.FontMultiLink
import casperix.ui.source.FontSource

class FontSupplier(val yDown: Boolean) : Disposable {
	companion object {
		var characterExpander:CharacterExpandByLocale? = CharacterExpandByLocale()
	}

	private val cache = mutableMapOf<FontSource, GdxFont>()
	private val generator = FontGenerator(yDown)

	fun get(font: FontSource, value: String): GdxFont {
		return get(font, value.map { it }.toSet())
	}

	fun get(source: FontSource, characters: Set<Char>): GdxFont {
		if (source !is FontLink && source !is FontMultiLink)throw Error("Unsupported font source: $source")

		val entry = cache.getOrPut(source) {
			val bitmapFont = generator.buildBitmapFont(source, characters)
			GdxFont(source, bitmapFont, characters)
		}

		if (!entry.symbols.containsAll(characters)) {
			val useCharacters = characterExpander?.expand(characters) ?: characters
			entry.symbols += useCharacters
			entry.bitmapFont = generator.buildBitmapFont(source, entry.symbols)
		}

		return entry
	}

	override fun dispose() {
		cache.values.forEach {
			it.bitmapFont.dispose()
		}
		cache.clear()
	}
}