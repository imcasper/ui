package casperix.ui.engine.font

import casperix.math.vector.Vector2d
import casperix.ui.engine.GlyphInfo
import casperix.ui.engine.TextLayout
import casperix.ui.engine.toHorizontalAlign
import casperix.ui.font.Font
import casperix.ui.font.FontMetrics
import casperix.ui.font.FontWeight
import casperix.ui.font.SymbolWidthMode
import casperix.ui.source.FontSource
import casperix.ui.type.LayoutAlign
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.utils.Align
import kotlin.math.absoluteValue

class GdxFont(override val source: FontSource, var bitmapFont: BitmapFont, var symbols: Set<Char>) : Font {
	override val name = bitmapFont.data.name
	override val size = bitmapFont.lineHeight
	override val weight =  FontWeight.NORMAL
	override val italic = false
	override val kerning = false

	override val metrics = FontMetrics(
		bitmapFont.lineHeight.absoluteValue.toDouble(),
		bitmapFont.capHeight.toDouble(),
		bitmapFont.xHeight.toDouble(),
		bitmapFont.ascent.absoluteValue.toDouble() + bitmapFont.xHeight.toDouble(),
		bitmapFont.descent.absoluteValue.toDouble(),
	)

	override fun calculateBounds(text:String, space: Vector2d, wrap:Boolean, symbolWidthMode: SymbolWidthMode): Vector2d {
		if (text.isEmpty()) return Vector2d.ZERO

		val textForTest = if (symbolWidthMode == SymbolWidthMode.DIGITS) {
			text.map { if (it.isDigit()) '0' else it }.joinToString("")
		} else if (symbolWidthMode == SymbolWidthMode.MONOSPACE) {
			text.map { 'W' }.joinToString("")
		} else {
			text
		}

		val layoutMaxW = GlyphLayout(bitmapFont, textForTest, Color.WHITE, space.x.toFloat(), Align.left, false)
		val layoutMaxH = if (wrap) GlyphLayout(bitmapFont, textForTest, Color.WHITE, space.x.toFloat(), Align.left, true) else layoutMaxW

		val maxWidth = layoutMaxW.runs.maxOfOrNull { it.width } ?: return Vector2d.ZERO
		val maxY = layoutMaxH.runs.maxOfOrNull { it.y } ?: return Vector2d.ZERO

		return Vector2d(maxWidth.toDouble(), (maxY + metrics.capHeight + metrics.descent))
	}

	override fun calculateLayout(text: String, space: Vector2d, wrap: Boolean, align:LayoutAlign): TextLayout {
		val gdxLayout = GlyphLayout(bitmapFont, text, Color.WHITE, space.x.toFloat(), align.horizontal.toHorizontalAlign(), wrap)
		val additionalY = bitmapFont.data.ascent + align.vertical.getPosition(space.y, gdxLayout.height.toDouble())

		var symbolIndex = 0
		var wordIndex = 0
		val glyphs = mutableListOf<GlyphInfo>()
		while (wordIndex < gdxLayout.runs.size) {
			val run: GlyphLayout.GlyphRun = gdxLayout.runs.get(wordIndex)
			val gdxGlyphs = run.glyphs
			val xAdvances = run.xAdvances
//			val color = run.color.toFloatBits()
			var gx = run.x.toDouble()
			val gy = run.y.toDouble()
			var glyphIndex = 0
			val nn = gdxGlyphs.size
			while (glyphIndex < nn) {
				val glyph = gdxGlyphs[glyphIndex]
				gx += xAdvances[glyphIndex]

				glyphs += GlyphInfo(symbolIndex++, Vector2d(gx, additionalY + gy), Vector2d(glyph.xoffset.toDouble(), glyph.yoffset.toDouble()), Vector2d(glyph.width.toDouble(), glyph.height.toDouble()))
//					glyph, gx, gy, color
				glyphIndex++
			}
			wordIndex++
		}

		return TextLayout(text, Vector2d(gdxLayout.width.toDouble(), gdxLayout.height.toDouble()), glyphs)
	}
}

