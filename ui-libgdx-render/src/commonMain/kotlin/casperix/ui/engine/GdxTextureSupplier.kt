package casperix.ui.engine

import casperix.map2d.IntMap2D
import casperix.math.axis_aligned.Box2i
import casperix.misc.Disposable
import casperix.math.vector.Vector2i
import casperix.math.color.Color
import casperix.misc.toByteBuffer
import casperix.ui.source.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.TextureData
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap
import com.badlogic.gdx.graphics.glutils.PixmapTextureData

class GdxTextureSupplier : Disposable {
	private val textureCache = mutableMapOf<BitmapSource, Texture>()
	private val bitmapCache = mutableMapOf<Pair<BitmapSource, Box2i>, TextureContainer>()
	private val unknownTexture = Texture(4, 4, Pixmap.Format.RGBA8888)

	fun remove(value: BitmapSource) {
		textureCache.keys.removeAll { source ->
			value == source
		}

		val removedItems = bitmapCache.filter { (key, container) ->
			value == key.first
		}
		removedItems.forEach { (key, container) ->
			container.dispose()
			bitmapCache.remove(key)
		}
	}



	fun get(source: BitmapSource, region: Box2i?, smooth: Boolean, mipMap: Boolean): TextureContainer {
		val texture = textureCache.getOrPut(source) {
			val texture = if (source is BitmapFile) {
				prepareTexture(Texture(source.file.toGdxFile(), mipMap), smooth, mipMap)
			} else if (source is BitmapData) {
				prepareTexture(Texture(source.map.toPixmap()), smooth, mipMap)
			} else {
				unknownTexture
			}
			texture
		}

		val actualRegion = region ?: Box2i.byDimension(Vector2i.ZERO, Vector2i(texture.width, texture.height))
		return bitmapCache.getOrPut(Pair(source, actualRegion)) {
			TextureContainer(source, texture, actualRegion)
		}

	}

	private fun prepareTexture(texture: Texture, smooth: Boolean, mipMap: Boolean): Texture {
		if (smooth) {
			texture.setAnisotropicFilter(4f)
		}
		if (mipMap) {
			if (smooth) {
				texture.setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.Linear)
			} else {
				texture.setFilter(TextureFilter.MipMapNearestNearest, TextureFilter.Nearest)
			}
		} else {
			if (smooth) {
				texture.setFilter(TextureFilter.Linear, TextureFilter.Linear)
			} else {
				texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
			}
		}

		return texture
	}


	override fun dispose() {
		bitmapCache.values.forEach {
			it.dispose()
		}
		bitmapCache.clear()
	}
}
