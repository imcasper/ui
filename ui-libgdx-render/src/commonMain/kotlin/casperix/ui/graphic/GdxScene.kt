package casperix.ui.graphic

import casperix.gdx.input.ProcessorInput
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.engine.Drawer
import casperix.ui.engine.GdxDrawer
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.AnimationController
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder

class GdxScene(node:UINode) : UIComponent(node), Graphic {
	val camera: PerspectiveCamera
	val modelBatch = ModelBatch()
	val environment = Environment()

	val cameraInput: CameraInputController

	val instances = mutableListOf<ModelInstance>()
	val controllers = mutableListOf<AnimationController>()

	init {
		node.graphic = this

		environment.set(ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f))
		environment.add(DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f))

		camera = PerspectiveCamera(67f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
		camera.position.set(7f, 7f, 7f)
		camera.lookAt(0f, 0f, 0f)
		camera.near = 1f
		camera.far = 300f
		camera.update()


		cameraInput = CameraInputController(camera)
		ProcessorInput(components, node.inputs, cameraInput)

		val modelBuilder = ModelBuilder()
		val model = modelBuilder.createBox(5f, 5f, 5f, Material(ColorAttribute.createDiffuse(Color.GREEN)), (VertexAttributes.Usage.Position or VertexAttributes.Usage.Normal).toLong())
		val instance = ModelInstance(model)
		instances += instance
	}

	override fun draw(drawer: Drawer, position: Vector2d, size: Vector2d) {
		val gdx = drawer as? GdxDrawer ?: throw Error("Support only GdxDrawer")

		val isDrawing = gdx.batch.isDrawing
		if (isDrawing) gdx.batch.end()

		val screenSize = Vector2i(Gdx.graphics.width, Gdx.graphics.height)
		val canvasPosition = position.roundToVector2i()
		val canvasSize = size.roundToVector2i()

		Gdx.gl.glViewport(canvasPosition.x, screenSize.y - canvasSize.y - canvasPosition.y, canvasSize.x, canvasSize.y)
		Gdx.gl.glClear(GL30.GL_DEPTH_BUFFER_BIT)

		controllers.forEach {
			it.update(Gdx.graphics.deltaTime)
		}

		cameraInput.update()
		modelBatch.begin(camera)
		modelBatch.render(instances, environment)
		modelBatch.end()


		Gdx.gl.glViewport(0, 0, screenSize.x, screenSize.y)

		if (isDrawing) gdx.batch.begin()
	}

}